import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import core.request.OperationType;
import org.apache.log4j.Logger;

public class LogData {
    private static Logger logger = Logger.getLogger(LogData.class);

    public final OperationType operation;
    public final JsonObject givenJsonObject;
    public final Long timestamp;

    public static final String TRANSFER_FORMAT = "%s - %s - %s";

    public LogData(OperationType operation, JsonObject givenJsonObject) {
        this.operation = operation;
        this.givenJsonObject = givenJsonObject;
        this.timestamp = System.currentTimeMillis();
    }

    public LogData(Long timestamp, OperationType operation, JsonObject givenJsonObject) {
        this.operation = operation;
        this.givenJsonObject = givenJsonObject;
        this.timestamp = timestamp;
    }

    public String convertToString() {
        return String.format(TRANSFER_FORMAT, System.currentTimeMillis(), this.operation.name(), this.givenJsonObject.toString());
    }

    public static LogData convertFromString(String data) {
        try {
            // 1402397944765 - CREATE_DOCUMENT - {"property1":true,"property2":8,"property3":"Hello"}
            String timestampAsString = data.substring(0, 13);
            Long timestamp = Long.parseLong(timestampAsString);
            String withoutTimestamp = data.substring(16);
            String[] split = withoutTimestamp.split(" - ");
            String operationTypeAsString = split[0];
            String jsonAsString = split[1];

            OperationType operationType = OperationType.getOperationType(operationTypeAsString);
            JsonObject jsonObject = parseJsonObject(jsonAsString);
            return new LogData(timestamp, operationType, jsonObject);
        } catch (RuntimeException e) {
            logger.debug("Error parsing journal", e);
            return null;
        }
    }

    private static JsonObject parseJsonObject(String jsonAsString) {
        JsonParser jsonParser = new JsonParser();
        return (JsonObject) jsonParser.parse(jsonAsString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LogData logData = (LogData) o;

        if (givenJsonObject != null ? !givenJsonObject.equals(logData.givenJsonObject) : logData.givenJsonObject != null)
            return false;
        if (operation != null ? !operation.equals(logData.operation) : logData.operation != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = operation != null ? operation.hashCode() : 0;
        result = 31 * result + (givenJsonObject != null ? givenJsonObject.hashCode() : 0);
        return result;
    }
}
