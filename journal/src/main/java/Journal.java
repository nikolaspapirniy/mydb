import com.google.gson.JsonObject;
import core.request.OperationType;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Journal {
    private static Logger logger = Logger.getLogger(Journal.class);

    public final String journalFilePath;
    private BufferedWriter appender;

    public Journal(String journalFilePath) throws IOException {
        this.journalFilePath = journalFilePath;
        this.appender = new BufferedWriter(new FileWriter(new File(journalFilePath), true));
    }

    public void storeToJournal(OperationType operation, JsonObject givenJsonObject) throws IOException {
        LogData logData = new LogData(operation, givenJsonObject);
        appender.write(logData.convertToString() + "\n");
        appender.flush();
    }

    public List<LogData> readAllFromJournal() {
        List<LogData> result = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(journalFilePath))) {
            String buf = null;
            while ((buf = bufferedReader.readLine()) != null) {
                LogData logData = LogData.convertFromString(buf);
                result.add(logData);
            }
            return result;
        } catch (IOException e) {
            throw new RuntimeException("Error reading journal", e);
        }
    }

    public boolean removeJournalFile() {
        try {
            if (appender != null)
                appender.close();
            File f = new File(journalFilePath);
            return f.delete();
        } catch (IOException e) {
            logger.warn("Error while deleting journal file = " + journalFilePath, e);
            return false;
        }
    }
}
