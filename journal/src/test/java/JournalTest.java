import com.google.gson.JsonObject;
import core.request.OperationType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class JournalTest {
    private Journal journal;

    @Before
    public void init() throws IOException {
        journal = new Journal("TestJournal");
    }

    @After
    public void tearDown() {
        journal.removeJournalFile();
    }

    @Test
    public void should_write_operation_to_journal_if_operation_exists() throws Exception {
        // given
        OperationType operationType = OperationType.CREATE_DOCUMENT;
        JsonObject givenJsonObject = new JsonObject();

        // when
        journal.storeToJournal(operationType, givenJsonObject);

        // then
        List<LogData> journalData = journal.readAllFromJournal();
        journalData.contains(new LogData(operationType, givenJsonObject));
    }
}
