import com.google.gson.JsonObject;
import com.nikolaspapirniy.operations.CreateDocument;
import core.request.OperationType;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LogDataTest {
    @Test
    public void should_convert_log_data_to_string_format() throws Exception {
        // given
        JsonObject givenJsonObject = new JsonObject();
        givenJsonObject.addProperty("property1", true);
        givenJsonObject.addProperty("property2", 8);
        givenJsonObject.addProperty("property3", "Hello");
        LogData logData = new LogData(OperationType.CREATE_DOCUMENT, givenJsonObject);

        // when
        String logDataAsString = logData.convertToString();
        System.out.println(logDataAsString);

        // then
        assertEquals(logData, LogData.convertFromString(logDataAsString));
    }
}
