package properties;

import org.junit.Before;
import org.junit.Test;

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class AppPropertiesTest {
    private Map<AppPropertyValues, Object> givenMap;

    @Before
    public void setUp() throws Exception {
        givenMap = new HashMap<AppPropertyValues, Object>();
    }

    @Test
    public void should_store_and_retrieve_property_as_string() throws Exception {
        // given
        AppPropertyValues key = AppPropertyValues.FOLDER_PATH;
        String expectedValue = "expected_value";
        givenMap.put(key, expectedValue);

        // when
        AppProperties.putProperties(givenMap);

        // then
        assertEquals(expectedValue, AppProperties.getPropertyAsString(key));
    }

    @Test(expected = AppPropertyFormatException.class)
    public void should_throw_ex_if_not_string() throws Exception {
        // given
        AppPropertyValues key = AppPropertyValues.FOLDER_PATH;
        List expectedValue = Arrays.asList("1", "2", "3");
        givenMap.put(key, expectedValue);

        // when
        AppProperties.putProperties(givenMap);

        //then
        AppProperties.getPropertyAsString(key);
    }

    @Test(expected = AppPropertyNotFoundException.class)
    public void should_throw_exception_if_null_string_property_requested() throws Exception {
        // when
        AppProperties.getPropertyAsString(null);
    }

    @Test
    public void should_store_and_retrieve_property_as_number() throws Exception {
        // given
        AppPropertyValues key = AppPropertyValues.HTTP_PORT;
        Long expectedValue = 8090L;
        givenMap.put(key, expectedValue);

        // when
        AppProperties.putProperties(givenMap);

        // then
        assertEquals(expectedValue, AppProperties.getPropertyAsNumber(key));
    }

    @Test(expected = AppPropertyNotFoundException.class)
    public void should_trow_exception_if_null_number_property_requested() throws Exception {
        // when
        AppProperties.getPropertyAsNumber(null);
    }

    @Test(expected = AppPropertyFormatException.class)
    public void should_throw_ex_if_not_number() throws Exception {
        // given
        AppPropertyValues key = AppPropertyValues.HTTP_PORT;
        List expectedValue = Arrays.asList("1", "2", "3");
        givenMap.put(key, expectedValue);

        // when
        AppProperties.putProperties(givenMap);

        //then
        AppProperties.getPropertyAsNumber(key);
    }

    @Test
    public void should_store_and_retrieve_property_as_list() throws Exception {
        // given
        AppPropertyValues key = AppPropertyValues.PACKAGES_WITH_CONTROLLERS;
        List expectedValue = Arrays.asList("1", "2", "3");
        givenMap.put(key, expectedValue);

        // when
        AppProperties.putProperties(givenMap);

        // then
        assertEquals(expectedValue, AppProperties.getPropertyAsList(key));
    }


    @Test(expected = AppPropertyNotFoundException.class)
    public void should_trow_exception_if_null_list_property_requested() throws Exception {
        // when
        AppProperties.getPropertyAsList(null);
    }

    @Test(expected = AppPropertyFormatException.class)
    public void should_throw_ex_if_not_list() throws Exception {
        // given
        AppPropertyValues key = AppPropertyValues.FOLDER_PATH;
        givenMap.put(key, "trahabr");

        // when
        AppProperties.putProperties(givenMap);

        //then
        AppProperties.getPropertyAsList(key);
    }

    @Test
    public void should_read_properties_from_file() throws Exception {
        // given
        Long expectedHttpValue = 7000L;

        // when
        AppProperties.loadFromFile(getFilePath("should_read_from_file.properties"));

        // then
        assertEquals(expectedHttpValue, AppProperties.getPropertyAsNumber(AppPropertyValues.HTTP_PORT));
    }

    private String getFilePath(String name) {
        URL resource = Thread.currentThread().getContextClassLoader().getResource(name);
        return resource.getPath().replaceAll("%20", " ");
    }

    @Test(expected = AppPropertyNotFoundException.class)
    public void should_trow_ex_if_wrong_property() throws Exception {
        // when
        AppProperties.loadFromFile(getFilePath("should_throw_ex_if_wrong_property.properties"));
    }

    @Test(expected = AppPropertyFormatException.class)
    public void should_throw_ex_if_property_in_wrong_format() throws Exception {
        // when
        AppProperties.loadFromFile(getFilePath("should_throw_ex_if_property_in_wrong_format.properties"));
    }
}








