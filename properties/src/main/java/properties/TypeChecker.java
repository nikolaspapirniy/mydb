package properties;

import java.util.List;

public class TypeChecker {
    public static void checkIsRightFormat(Object property, Class clazz) {
        if (String.class.equals(clazz))
            checkIsString(property);
        else if (List.class.equals(clazz))
            checkIsList(property);
        else if (Number.class.equals(clazz))
            checkIsNumber(property);
        else
            throw new RuntimeException("Not supported format");
    }

    private static void checkIsNumber(Object property) {
        if (!(property instanceof Long) && !(property instanceof Integer))
            throw new AppPropertyFormatException();
    }

    public static void checkIsList(Object property) {
        if (!(property instanceof List))
            throw new AppPropertyFormatException();
    }

    public static void checkIsString(Object property) {
        if (!(property instanceof String))
            throw new AppPropertyFormatException();
    }
}
