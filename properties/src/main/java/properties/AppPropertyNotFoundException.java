package properties;

public class AppPropertyNotFoundException extends RuntimeException {
    public AppPropertyNotFoundException() {
        super();
    }

    public AppPropertyNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public AppPropertyNotFoundException(String message) {
        super(message);
    }
}
