package properties;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class AppProperties {
    private static Map<AppPropertyValues, Object> properties = new ConcurrentHashMap<AppPropertyValues, Object>();

    static {
        loadDefaultProperties();
    }

    public static String getPropertyAsString(AppPropertyValues name) {
        return (String) getProperty(name, String.class);
    }

    private static void assertPropertyNotNull(Object property) {
        if (isNull(property))
            throw new AppPropertyNotFoundException();
    }

    public static void putProperties(Map<AppPropertyValues, Object> externalProperties) {
        for (Map.Entry<AppPropertyValues, Object> object : externalProperties.entrySet()) {
            properties.put(object.getKey(), object.getValue());
        }
    }

    private static boolean isNull(Object property) {
        return property == null;
    }

    public static Long getPropertyAsNumber(AppPropertyValues name) {
        assertPropertyNotNull(name);
        Object property = properties.get(name);
        return getValuesAsLong(property);
    }

    private static Long getValuesAsLong(Object property) {
        if (String.class.equals(property.getClass()))
            return Long.valueOf(property.toString());
        else if (Long.class.equals(property.getClass()) || Integer.class.equals(property.getClass()))
            return ((Number) property).longValue();
        else
            throw new AppPropertyFormatException(String.format("Property %s in wrong format", property));
    }

    private static Object getProperty(AppPropertyValues name, Class clazz) {
        assertPropertyNotNull(name);
        Object property = properties.get(name);
        TypeChecker.checkIsRightFormat(property, clazz);
        return property;
    }

    public static List getPropertyAsList(AppPropertyValues name) {
        return (List) getProperty(name, List.class);
    }

    public static void loadDefaultProperties() {
        for (Map.Entry<AppPropertyValues, Object> e : DefaultValues.defaultValues.entrySet())
            properties.put(e.getKey(), e.getValue());
    }

    public static void loadFromFile(String fileName) {
        Properties propertiesFromFile = loadPropertiesFromFile(fileName);
        addAllPropertiesToMap(propertiesFromFile);
    }

    private static void addAllPropertiesToMap(Properties propertiesFromFile) {
        for (Map.Entry<Object, Object> o : propertiesFromFile.entrySet())
            addPropertyToMap(o);
    }

    private static void addPropertyToMap(Map.Entry<Object, Object> o) {
        String key = ((String) o.getKey()).toUpperCase();
        try {
            AppPropertyValues appPropertyValue = AppPropertyValues.valueOf(key);
            appPropertyValue.validateType(o.getValue().toString());
            properties.put(appPropertyValue, o.getValue());
        } catch (IllegalArgumentException e) {
            throw new AppPropertyNotFoundException(String.format("Property %s is wrong", key), e);
        }
    }

    private static Properties loadPropertiesFromFile(String fileName) {
        try(InputStream is = new FileInputStream(fileName)) {
            Properties result = new Properties();
            result.load(is);
            return result;
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Properties file not found", e);
        } catch (IOException e) {
            throw new RuntimeException("Error during reading properties file", e);
        }
    }

    public static class DefaultValues {
        final static Map<AppPropertyValues, Object> defaultValues = new ConcurrentHashMap<AppPropertyValues, Object>();

        static {
            defaultValues.put(AppPropertyValues.FOLDER_PATH, "internal_data");
            defaultValues.put(AppPropertyValues.FOLDER_PREFIX, "");
            defaultValues.put(AppPropertyValues.INDEX_ENDING, "_idx");
            defaultValues.put(AppPropertyValues.STATIC_CONTENT_FOLDER, "static");
            defaultValues.put(AppPropertyValues.LINE_SEPARATOR, System.getProperty("line.separator"));
            defaultValues.put(AppPropertyValues.PATH_SEPARATOR, File.separator);
            defaultValues.put(AppPropertyValues.DEFAULT_STATIC_CONTENT_PATH, getDefaultStaticContentFolder());
            defaultValues.put(AppPropertyValues.STATIC_CONTENT_FOLDERS_TO_SCAN, Arrays.asList(defaultValues.get(AppPropertyValues.DEFAULT_STATIC_CONTENT_PATH)));
            defaultValues.put(AppPropertyValues.HTTP_PORT, 8080);
            defaultValues.put(AppPropertyValues.PACKAGES_WITH_CONTROLLERS, Arrays.asList("network.http.controllers"));
            defaultValues.put(AppPropertyValues.PACKAGES_WITH_FILTERS, Arrays.asList("network.http.filters"));
            defaultValues.put(AppPropertyValues.HTTP_PARAMETERS_ATTRIBUTE, "parameters");
        }

        private static Object getDefaultStaticContentFolder() {
            StringBuilder sb = new StringBuilder();
            sb.append(defaultValues.get(AppPropertyValues.FOLDER_PATH));
            sb.append(defaultValues.get(AppPropertyValues.PATH_SEPARATOR));
            sb.append(defaultValues.get(AppPropertyValues.STATIC_CONTENT_FOLDER));
            return sb.toString();
        }
    }

}