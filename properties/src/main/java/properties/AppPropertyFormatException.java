package properties;

public class AppPropertyFormatException extends RuntimeException {
    public AppPropertyFormatException() {
        super();
    }

    public AppPropertyFormatException(String message) {
        super(message);
    }
}
