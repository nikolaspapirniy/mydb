package properties;

import java.util.Arrays;
import java.util.List;
import java.util.regex.PatternSyntaxException;

public enum AppPropertyValues {
    FOLDER_PATH(String.class),
    LINE_SEPARATOR(String.class),
    PATH_SEPARATOR(String.class),
    INDEX_ENDING(String.class),
    FOLDER_PREFIX(String.class),
    HTTP_PORT(Long.class),
    PACKAGES_WITH_FILTERS(List.class),
    PACKAGES_WITH_CONTROLLERS(List.class),
    HTTP_PARAMETERS_ATTRIBUTE(String.class),
    DEFAULT_STATIC_CONTENT_PATH(String.class),
    STATIC_CONTENT_FOLDER(String.class),
    STATIC_CONTENT_FOLDERS_TO_SCAN(String.class);
    private static final String NOT_VALID_FORMAT_MSG = " is not valid format";

    private Class clazz;

    AppPropertyValues(Class clazz) {
        this.clazz = clazz;
    }

    public void validateType(String value) {
        String errorMessage = value + NOT_VALID_FORMAT_MSG;
        if (clazz.equals(Integer.class))
            assertThatInteger(value, errorMessage);
        else if (clazz.equals(Long.class))
            assertThatLong(value, errorMessage);
        else if (clazz.equals(List.class))
            assertThatList(value, errorMessage);
    }

    private void assertThatList(String value, String errorMessage) {
        try {
            String[] split = value.split(",");
            if (split.length == 0)
                throw new AppPropertyFormatException(errorMessage);
            else
                Arrays.asList(split);
        } catch (PatternSyntaxException e) {
            throw new AppPropertyFormatException(errorMessage);
        }
    }

    private void assertThatLong(String value, String errorMessage) {
        try {
            Long.valueOf(value);
        } catch (NumberFormatException e) {
            throw new AppPropertyFormatException(errorMessage);
        }
    }

    private void assertThatInteger(String value, String errorMessage) {
        try {
            Integer.valueOf(value);
        } catch (NumberFormatException e) {
            throw new AppPropertyFormatException(errorMessage);
        }
    }
}


