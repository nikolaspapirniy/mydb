package com.nikolaspapirniy.operations;

import com.google.gson.JsonObject;
import core.request.OperationHandler;
import core.request.OperationType;
import core.request.RequestParameter;
import org.junit.Before;
import org.junit.Test;
import repository.Collection;

import static org.junit.Assert.assertNotNull;

public class CreateDocumentTest extends AbstractOperationTest {
    private String collectionName;
    private Collection collection;

    protected Operation operation = new CreateDocument();
    private JsonObject request;

    @Before
    public void setUp() throws Exception {
        initRepository();
        collectionName = "new_collection" + System.currentTimeMillis();
        collection = repository.createCollection(collectionName);

        request = new JsonObject();
        request.addProperty(OperationHandler.JSON_REQUEST_TYPE, OperationType.CREATE_DOCUMENT.name());
    }

    @Test
    public void should_create_document() throws Exception {
        // given
        JsonObject document = new JsonObject();
        document.addProperty("name", "vasia");
        document.addProperty("age", 25);

        request.add(RequestParameter.DOCUMENT_PARAMETER, document);
        request.addProperty(RequestParameter.COLLECTION_PARAMETER, collectionName);

        // when
        JsonObject resultAsJson = getJsonFromResponse(operation.processOperation(request));

        // then
        assertStatusOk(resultAsJson);
        assertNotNull(collection.retrieve(Long.parseLong(getResultMessage(resultAsJson))));
    }
}