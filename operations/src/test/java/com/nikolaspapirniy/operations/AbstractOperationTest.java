package com.nikolaspapirniy.operations;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import core.response.ResponseStatus;
import core.response.ResponseTags;
import org.junit.After;
import repository.Repository;
import repository.RepositoryHolder;

import static org.junit.Assert.assertEquals;

public abstract class AbstractOperationTest {
    protected static Repository repository;
    protected JsonParser parser = new JsonParser();

    @After
    public void tearDown() throws Exception {
        repository.removeAllCollections();
    }

    protected static void initRepository() {
        repository = RepositoryHolder.getRepository();
    }

    protected JsonObject getJsonFromResponse(String result) {
        return (JsonObject) parser.parse(result);
    }

    protected boolean hasNoErrors(JsonObject resultAsJson) {
        return resultAsJson.get(ResponseTags.ERRORS.getTag()).getAsString().isEmpty();
    }

    protected boolean hasErrors(JsonObject resultAsJson) {
        return !hasNoErrors(resultAsJson);
    }

    protected static class RepositoryMock extends Repository {
    }

    protected void assertStatusOk(JsonObject resultAsJson) {
        assertEquals(ResponseStatus.OK.name(), resultAsJson.get(ResponseTags.STATUS.getTag()).getAsString());
    }

    protected void assertStatusFail(JsonObject resultAsJson) {
        assertEquals(ResponseStatus.FAIL.name(), resultAsJson.get(ResponseTags.STATUS.getTag()).getAsString());
    }

    protected String getResultMessage(JsonObject resultAsJson) {
        return resultAsJson.get(ResponseTags.RESULT_MESSAGE.getTag()).getAsString();
    }
}
