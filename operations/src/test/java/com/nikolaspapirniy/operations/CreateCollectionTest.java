package com.nikolaspapirniy.operations;

import com.google.gson.JsonObject;
import core.request.OperationHandler;
import core.request.OperationType;
import core.request.RequestParameter;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class CreateCollectionTest extends AbstractOperationTest {

    protected Operation operation = new CreateCollection();
    private JsonObject request;

    @Before
    public void setUp() throws Exception {
        initRepository();
        request = new JsonObject();
        request.addProperty(OperationHandler.JSON_REQUEST_TYPE, OperationType.CREATE_COLLECTION.name());
    }

    @Test
    public void should_handle_create_collection_request() throws Exception {
        // given
        String newCollectionName = "new_collection_name_from_test" + System.currentTimeMillis();
        request.addProperty(RequestParameter.COLLECTION_PARAMETER, newCollectionName);

        // when
        JsonObject resultAsJson = getJsonFromResponse(operation.processOperation(request));

        // then
        assertStatusOk(resultAsJson);
        assertTrue(repository.hasCollection(newCollectionName));
    }

    @Test
    public void should_not_create_collection_if_no_collection_name() throws Exception {
        // given

        // when
        JsonObject resultAsJson = getJsonFromResponse(operation.processOperation(request));

        // then
        assertStatusFail(resultAsJson);
        assertTrue(hasErrors(resultAsJson));
    }
}
