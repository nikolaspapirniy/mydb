package com.nikolaspapirniy.operations.utils;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import core.request.RequestParameter;

public class JsonUtils {
    public static final String VALUE_PROPERTY = "value";

    public static JsonObject getDocument(JsonObject json) {
        try {
            return tryToParseDocumentString(json);
        } catch (ClassCastException e) {
            return getDocumentAsString(json);
        }
    }

    private static JsonObject getDocumentAsString(JsonObject json) {
        JsonObject result = new JsonObject();
        String documentAsString;
        try {
            documentAsString = json.getAsJsonPrimitive(RequestParameter.DOCUMENT_PARAMETER).getAsString();
        } catch (ClassCastException e) {
            documentAsString = json.getAsJsonObject(RequestParameter.DOCUMENT_PARAMETER).toString();
        }
        result.addProperty(VALUE_PROPERTY, documentAsString);
        return result;
    }

    private static JsonObject tryToParseDocumentString(JsonObject json) {
        String documentAsString = json.getAsJsonPrimitive(RequestParameter.DOCUMENT_PARAMETER).getAsString();
        JsonObject documentAsJson = (JsonObject) new JsonParser().parse(documentAsString);
        return documentAsJson;
    }
}
