package com.nikolaspapirniy.operations;

import com.google.gson.JsonObject;
import core.request.OperationType;
import core.request.RequestParameter;
import core.response.ResponseErrors;
import org.apache.log4j.Logger;
import repository.Collection;

public class RetrieveDocument extends AbstractOperation {

    private static Logger logger = Logger.getLogger(RetrieveAllDocuments.class);

    public OperationType getRequestType() {
        return OperationType.RETRIEVE_DOCUMENT;
    }

    public String processOperationSpecific(JsonObject json, ResponseErrors errors) {
        Long documentIdParameter = json.getAsJsonPrimitive(RequestParameter.DOCUMENT_ID).getAsLong();
        String collectionParameter = json.getAsJsonPrimitive(RequestParameter.COLLECTION_PARAMETER).getAsString();

        if (!repository.hasCollection(collectionParameter))
            return "Collections doesn't exist";

        Collection collection = repository.getCollection(collectionParameter);

        JsonObject retrieve = collection.retrieve(documentIdParameter);
        if (retrieve == null) {
            logger.debug(String.format("No record found in [collection=%s] with [id=%s]", collectionParameter, documentIdParameter));
            return "No record found";
        } else {
            return retrieve.toString();
        }
    }
}
