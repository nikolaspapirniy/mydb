package com.nikolaspapirniy.operations;

import com.google.gson.JsonObject;
import com.nikolaspapirniy.operations.utils.JsonUtils;
import core.request.OperationType;
import core.request.RequestParameter;
import core.response.ResponseErrors;
import org.apache.log4j.Logger;
import repository.Collection;

public class UpdateDocument extends AbstractOperation {

    private static Logger logger = Logger.getLogger(UpdateDocument.class);

    public OperationType getRequestType() {
        return OperationType.UPDATE_DOCUMENT;
    }

    public String processOperationSpecific(JsonObject json, ResponseErrors errors) {
        Long documentIdParameter = json.getAsJsonPrimitive(RequestParameter.DOCUMENT_ID).getAsLong();
        String collectionParameter = json.getAsJsonPrimitive(RequestParameter.COLLECTION_PARAMETER).getAsString();
        JsonObject newDocument = JsonUtils.getDocument(json);

        if (!repository.hasCollection(collectionParameter))
            return "Collections doesn't exist";

        Collection collection = repository.getCollection(collectionParameter);

        collection.update(documentIdParameter, newDocument);
        return "Record updated";
    }
}
