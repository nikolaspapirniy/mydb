package com.nikolaspapirniy.operations;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nikolaspapirniy.operations.utils.JsonUtils;
import core.request.OperationType;
import core.request.RequestParameter;
import core.response.ResponseErrors;
import org.apache.log4j.Logger;
import repository.Collection;

public class CreateDocument extends AbstractOperation {
    private static Logger logger = Logger.getLogger(CreateDocument.class);

    public OperationType getRequestType() {
        return OperationType.CREATE_DOCUMENT;
    }

    public String processOperationSpecific(JsonObject json, ResponseErrors errors) {
        JsonObject document = JsonUtils.getDocument(json);
        String collectionParameter = json.getAsJsonPrimitive(RequestParameter.COLLECTION_PARAMETER).getAsString();

        if (!repository.hasCollection(collectionParameter))
            return "Collections doesn't exist";

        Collection collection = repository.getCollection(collectionParameter);

        long id = collection.insert(document);
        return Long.toString(id);
    }
}
