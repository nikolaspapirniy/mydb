package com.nikolaspapirniy.operations;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import core.request.OperationType;
import core.request.RequestParameter;
import core.response.ResponseErrors;
import org.apache.log4j.Logger;
import repository.Collection;

public class DeleteDocument extends AbstractOperation {

    private static Logger logger = Logger.getLogger(DeleteDocument.class);

    public OperationType getRequestType() {
        return OperationType.DELETE_DOCUMENT;
    }

    public String processOperationSpecific(JsonObject json, ResponseErrors errors) {
        Long documentIdParameter = json.getAsJsonPrimitive(RequestParameter.DOCUMENT_ID).getAsLong();
        String collectionParameter = json.getAsJsonPrimitive(RequestParameter.COLLECTION_PARAMETER).getAsString();

        if (!repository.hasCollection(collectionParameter))
            return "Collections doesn't exist";

        Collection collection = repository.getCollection(collectionParameter);

        collection.delete(documentIdParameter);
        logger.debug(String.format("Deleted document from [collection=%s] and [id=%s]", collectionParameter, documentIdParameter));
        return "Document deleted";
    }
}
