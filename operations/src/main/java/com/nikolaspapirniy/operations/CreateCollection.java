package com.nikolaspapirniy.operations;

import com.google.gson.JsonObject;
import core.request.OperationType;
import core.request.RequestParameter;
import core.response.ResponseErrors;
import org.apache.log4j.Logger;
import repository.Collection;

public class CreateCollection extends AbstractOperation {
    private static Logger logger = Logger.getLogger(CreateCollection.class);

    @Override
    public String processOperationSpecific(JsonObject json, ResponseErrors errors) {
        String collectionName = json.getAsJsonPrimitive(RequestParameter.COLLECTION_PARAMETER).getAsString();

        Collection createdCollection = repository.createCollection(collectionName);
        if (createdCollection == null) {
            errors.addError("Collection already exists");
            return null;
        } else {
            String resultMessage = String.format("Collection %s created!", collectionName);
            logger.debug(resultMessage);
            return collectionName;
        }
    }

    @Override
    public OperationType getRequestType() {
        return OperationType.CREATE_COLLECTION;
    }
}
