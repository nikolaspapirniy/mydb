package com.nikolaspapirniy.operations;


import com.google.gson.JsonObject;
import core.request.OperationType;
import core.response.ResponseErrors;
import core.response.ResponseStatus;
import core.response.ResponseTags;
import org.apache.commons.lang.StringUtils;
import repository.Repository;
import repository.RepositoryHolder;

import java.util.List;

public abstract class AbstractOperation implements Operation {
    protected final Repository repository = RepositoryHolder.getRepository();

    public abstract String processOperationSpecific(JsonObject json, ResponseErrors errors);

    public String processOperation(JsonObject json) {
        JsonObject resultObject = new JsonObject();
        ResponseErrors errors = validatePresentsOfRequiredFields(json);
        if (errors.hasErrors())
            return getErrorResult(resultObject, errors);

        String processOperationSpecificResult = processOperationSpecific(json, errors);

        addResponseMessage(resultObject, processOperationSpecificResult);
        addResultStatus(resultObject, errors);
        addErrorMessages(resultObject, errors);

        return resultObject.toString();
    }

    private void addResponseMessage(JsonObject resultObject, String processResult) {
        if (!StringUtils.isEmpty(processResult))
            resultObject.addProperty(ResponseTags.RESULT_MESSAGE.getTag(), processResult);
    }

    private void addErrorMessages(JsonObject resultObject, ResponseErrors errors) {
        if (errors.hasErrors())
            resultObject.addProperty(ResponseTags.ERRORS.getTag(), errors.toString());
    }

    private String getErrorResult(JsonObject resultObject, ResponseErrors errors) {
        resultObject.addProperty(ResponseTags.ERRORS.getTag(), errors.toString());
        resultObject.addProperty(ResponseTags.STATUS.getTag(), ResponseStatus.FAIL.name());
        return resultObject.toString();
    }

    private void addResultStatus(JsonObject resultObject, ResponseErrors errors) {
        if (errors.hasErrors())
            resultObject.addProperty(ResponseTags.STATUS.getTag(), ResponseStatus.FAIL.name());
        else
            resultObject.addProperty(ResponseTags.STATUS.getTag(), ResponseStatus.OK.name());
    }

    private ResponseErrors validatePresentsOfRequiredFields(JsonObject json) {
        OperationType operationType = getRequestType();
        List<String> requiredFields = operationType.getRequiredFields();
        ResponseErrors errors = new ResponseErrors();
        for (String requiredField : requiredFields) {
            if (!json.has(requiredField))
                errors.addError("No required field: " + requiredField);
        }
        return errors;
    }
}
