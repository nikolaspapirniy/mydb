package com.nikolaspapirniy.operations;

import com.google.gson.JsonObject;
import core.request.OperationType;

public interface Operation {

    OperationType getRequestType();

    String processOperation(JsonObject json);
}
