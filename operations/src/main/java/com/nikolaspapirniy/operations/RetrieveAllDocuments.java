package com.nikolaspapirniy.operations;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import core.request.OperationType;
import core.request.RequestParameter;
import core.response.ResponseErrors;
import org.apache.log4j.Logger;
import repository.Collection;

import java.util.List;

public class RetrieveAllDocuments extends AbstractOperation {

    private static Logger logger = Logger.getLogger(RetrieveAllDocuments.class);

    public OperationType getRequestType() {
        return OperationType.RETRIEVE_ALL_DOCUMENTS;
    }

    public String processOperationSpecific(JsonObject json, ResponseErrors errors) {
        String collectionParameter = json.getAsJsonPrimitive(RequestParameter.COLLECTION_PARAMETER).getAsString();

        if (!repository.hasCollection(collectionParameter))
            return "Collections doesn't exist";

        Collection collection = repository.getCollection(collectionParameter);

        List<JsonObject> result = collection.retrieveAll();
        if (result == null) {
            return "No records";
        } else {
            return new Gson().toJson(result);
        }
    }
}
