package core.response;

public enum ResponseTags {
    RESULT_MESSAGE("message"),
    ERRORS("errors"),
    STATUS("status");

    private final String tag;

    private ResponseTags(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }
}
