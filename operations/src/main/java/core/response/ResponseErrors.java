package core.response;

import java.util.ArrayList;
import java.util.List;

public class ResponseErrors {
    private List<ResponseError> errors = new ArrayList<ResponseError>();

    public void addError(ResponseError error) {
        errors.add(error);
    }

    public void addError(String error) {
        errors.add(new ResponseError(error));
    }

    public boolean hasErrors() {
        return !errors.isEmpty();
    }

    @Override
    public String toString() {
        if (errors.isEmpty())
            return "No errors";
        else
            return errorsToString();
    }

    private String errorsToString() {
        return errors.toString();
    }
}
