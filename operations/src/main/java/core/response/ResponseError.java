package core.response;

public class ResponseError {
    private final String cause;

    public ResponseError(String cause) {
        this.cause = cause;
    }

    public String getCause() {
        return cause;
    }

    @Override
    public String toString() {
        return cause;
    }
}
