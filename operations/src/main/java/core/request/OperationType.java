package core.request;

import com.google.gson.JsonObject;
import com.nikolaspapirniy.operations.*;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;

public enum OperationType {
    NO_OPERATION(NoOperationHandler.class),
    CREATE_COLLECTION(CreateCollection.class,
            RequestParameter.COLLECTION_PARAMETER),
    CREATE_DOCUMENT(CreateDocument.class,
            RequestParameter.DOCUMENT_PARAMETER, RequestParameter.COLLECTION_PARAMETER),
    DELETE_DOCUMENT(DeleteDocument.class,
            RequestParameter.COLLECTION_PARAMETER, RequestParameter.DOCUMENT_ID),
    RETRIEVE_DOCUMENT(RetrieveDocument.class,
            RequestParameter.COLLECTION_PARAMETER, RequestParameter.DOCUMENT_ID),
    RETRIEVE_ALL_DOCUMENTS(RetrieveAllDocuments.class,
            RequestParameter.COLLECTION_PARAMETER),
    UPDATE_DOCUMENT(UpdateDocument.class,
            RequestParameter.COLLECTION_PARAMETER, RequestParameter.DOCUMENT_ID, RequestParameter.DOCUMENT_PARAMETER);

    private static Logger logger = Logger.getLogger(OperationType.class);

    private final List<String> requiredFields;
    private final Class handler;

    private OperationType(Class handler, String... requiredFields) {
        this.handler = handler;
        this.requiredFields = Arrays.asList(requiredFields);
    }

    public Operation createNewHandler() {
        try {
            return (Operation) handler.newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public static OperationType getOperationType(String requestType) {
        try {
            return OperationType.valueOf(requestType.toUpperCase());
        } catch (IllegalArgumentException e) {
            return NO_OPERATION;
        }
    }

    private static class NoOperationHandler implements Operation {
        @Override
        public OperationType getRequestType() {
            return OperationType.NO_OPERATION;
        }

        @Override
        public String processOperation(JsonObject json) {
            return "OPERATION NOT SUPPORTED";
        }
    }

    public List<String> getRequiredFields() {
        return requiredFields;
    }
}