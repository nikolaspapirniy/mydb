package core.request;

public class RequestParameter {
    public static final String DOCUMENT_ID = "document_id";
    public static final String COLLECTION_PARAMETER = "collection";
    public static final String DOCUMENT_PARAMETER = "document";
}
