package core.request;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.nikolaspapirniy.operations.Operation;

import java.util.Map;

public class OperationHandler {

    public static final String JSON_REQUEST_TYPE = "requestType";

    public String handle(Map map) {
        Gson gson = new Gson();
        JsonElement paramsAsJson = gson.toJsonTree(map, Map.class);
        return handle((JsonObject) paramsAsJson);
    }

    public String handle(JsonObject jsonObject) {
        Operation operation = defineRequestType(jsonObject);
        String result = operation.processOperation(jsonObject);
        return result;
    }

    private Operation defineRequestType(JsonObject json) {
        String requestTypeFromJson = json.getAsJsonPrimitive(JSON_REQUEST_TYPE).getAsString().toUpperCase();
        OperationType operationType = OperationType.getOperationType(requestTypeFromJson);
        if (operationType == OperationType.NO_OPERATION)
            return null;
        else
            return operationType.createNewHandler();
    }
}
