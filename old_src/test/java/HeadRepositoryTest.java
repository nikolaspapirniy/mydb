import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.Test;
import repository.HeadRepository;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class HeadRepositoryTest {

    private HeadRepository repository;
    private static final String testName = "test.txt";

    static {
        BasicConfigurator.configure();
    }

    @Before
    public void init() {
        repository = HeadRepository.getInstance();
    }

    @Test
    public void appendTest() {
        File f = new File(testName);
        Map<String, String> map = new HashMap<String, String>();
        Map<String, String> result = null;

        map.put("1", "one");
        map.put("2", "two");
        map.put("3", "three");
        HeadRepository.RepositoryUtils.writeParamsToFile(f, map);
        Map<String, String> map2 = new HashMap<String, String>();
        map2.put("4", "four");
        map2.put("5", "five");
        HeadRepository.RepositoryUtils.appendParamsToFile(f, map2);

        try {
            result = HeadRepository.RepositoryUtils.readParamsFromFile(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        map.putAll(map2);
        assertEquals(map, result);
    }

    @Test
    public void test_crud() {
        assertTrue(repository.create("0", "zero"));
        assertTrue(repository.create("1", "one"));
        assertTrue(repository.create("2", "two"));
        assertFalse(repository.create("2", "two"));
        assertTrue(repository.create("3", "three"));
        assertEquals(repository.retrieve("0"), "zero");
        assertEquals(repository.delete("0"), "zero");
        assertNull(repository.delete("0"));
        assertNull(repository.retrieve("0"));
        assertEquals(repository.update("3", "new_three"), "three");
        assertEquals(repository.retrieve("3"), "new_three");
    }

    @Test
    public void test_commit_snapshot() {
        repository.create("0", "zero");
        repository.create("1", "one");
        repository.create("2", "two");
        int before = repository.getWorkingRevision().getWorkingRevisionNumber();
        int revisionNumber = repository.commit();
        assertEquals(revisionNumber, before + 1);
        int newRevisionNumber = repository.commit();
        assertEquals(revisionNumber, newRevisionNumber);
        repository.delete("0");
        repository.delete("1");
        repository.delete("2");
        repository.snapshot(revisionNumber);
        System.out.println(repository.getWorkingRevision().getWorkingMap());
        assertEquals(repository.retrieve("0"), "zero");
        assertEquals(repository.retrieve("1"), "one");
        assertEquals(repository.retrieve("2"), "two");
    }

    @Test
    public void test() {
        repository.delete("0");
        repository.create("1", "one");
        repository.create("2", "two");
        // repository.create("3", "three");
        int revisionNumber = repository.commit();
        System.out.println(revisionNumber);
    }

    @Test
    public void test1() {
        repository.getSystemConfig().setHeadRevisionNumber(100);
        repository.getSystemConfig().flushConfig();
    }

    @Test
    public void test2() {
        System.out.println(repository.availableSnapshots());

    }
}
