package network.http.handlers;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.log4j.Logger;
import repository.HeadRepository;
import repository.old.static_content.StaticContentManager;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractHandler implements HttpHandler {

    private static Logger logger = Logger.getLogger(AbstractHandler.class);
    protected static StaticContentManager staticContentManager = StaticContentManager.getInstance();
    protected Map<String, Object> params = new HashMap<String, Object>();

    static {
        initStaticContent();
    }

    public static void initStaticContent() {
        // @TODO search for file through init catalogues
        staticContentManager.addTextEntityToManager("temp.html", HeadRepository.STATIC_FILES_FOLDER_NAME + File.separator + "temp.html");
        staticContentManager.addTextEntityToManager("test_page.html", HeadRepository.STATIC_FILES_FOLDER_NAME + File.separator + "test_page.html");
    }

    public void handle(HttpExchange exchange) throws IOException {
        params = (Map<String, Object>) exchange.getAttribute("parameters");

        Map<String, String> elParams = new HashMap<String, String>();
        String pageName = businessLogicProcess(elParams);
        String responseString = renderPage(pageName, elParams);

        exchange.sendResponseHeaders(200, responseString.length());
        OutputStream os = exchange.getResponseBody();
        os.write(responseString.getBytes());
        os.close();

    }

    /**
     * Renders page with el params
     *
     * @param elParams params to render with
     * @return answer in html form
     */
    private String renderPage(String pageName, Map<String, String> elParams) {
        // staticContentManager.getTextEntityFromManager("temp.html");
        String resultString = staticContentManager.getTextEntityFromManager(pageName);
        if (resultString == null) {
            String errorMessage = "File " + pageName + " not found!";
            logger.error(errorMessage);
            return errorMessage;
        } else {
            String resultStringWithReplacedEl = HandlerUtils.replaceElToValues(resultString, elParams);
            return resultStringWithReplacedEl;
        }
    }

    /**
     * Processing all businesslogic of a single handler
     *
     * @param elParams expression languaes paramethers which will be used to render
     *                 page in style ${param}
     * @return page name to render
     */
    public abstract String businessLogicProcess(Map<String, String> elParams);

}
