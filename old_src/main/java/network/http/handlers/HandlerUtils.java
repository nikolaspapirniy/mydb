package network.http.handlers;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HandlerUtils {

    /**
     * Replaces expression languages forms to a simple text ${myparam}
     *
     * @param text          where to look for el constuctions
     * @param replaceParams map param values associated with param names
     * @return string with replaced el constructions
     */
    public static String replaceElToValues(String text, Map<String, String> replaceParams) {
        return replaceElToValues(text, replaceParams, "null");
    }

    /**
     * Replaces expression languages forms to a simple text ${myparam}
     *
     * @param text           where to look for el constuctions
     * @param replaceParams  map param values associated with param names
     * @param noParamMessage string to be replace to if there is no param for el value
     * @return string with replaced el constructions
     */
    public static String replaceElToValues(String text, Map<String, String> replaceParams, String noParamMessage) {
        StringBuilder builder = new StringBuilder(text);
        // for(Map.Entry<String, String> entry : replaceParams.entrySet()) {
        // builder.replaceAll(entry.getKey(), entry.getValue());
        // }
        Pattern pattern = Pattern.compile("\\$\\{(\\S)+?\\}");
        Matcher matcher = pattern.matcher(text);

        // delta in string builder and param string (after replacing)
        int generalDelta = 0;
        // delta between found character indexces (${found_param})
        int foundDelta = 0;

        // start index of found word
        int start = 0;
        // end index of found word
        int end = 0;
        // SB for deleting pre and post characters like ${ and }
        StringBuilder foundStringBuilder = new StringBuilder();
        while (matcher.find()) {
            // crear sb
            foundStringBuilder.delete(0, foundStringBuilder.length());
            start = matcher.start();
            end = matcher.end();

            // deleting extra characters ${ and }
            foundStringBuilder.append(matcher.group());
            foundStringBuilder.delete(0, 2);
            foundStringBuilder.delete(foundStringBuilder.length() - 1, foundStringBuilder.length());

            String paramStr = replaceParams.get(foundStringBuilder.toString());
            // in case there is no param
            paramStr = (paramStr == null) ? noParamMessage : paramStr;

            builder.replace(generalDelta + start, generalDelta + end, paramStr);

            foundDelta = paramStr.length() - (end - start);
            generalDelta += foundDelta;
        }
        return builder.toString();
    }

    public static void main(String[] args) {
        String query = "0123456 ${privet}, ${poka}, ${do_svidaniya} this is continue asd asd${sd } asd";
        HashMap<String, String> hashMap = new HashMap<String, String>();
        // hashMap.put("privet", "privet-privet");
        hashMap.put("poka", "poka-poka");
        hashMap.put("do_svidaniya", "do_svidaniya-do_svidaniya");
        String replaceElToValues = replaceElToValues(query, hashMap, "!");
        System.out.println(query);
        System.out.println(replaceElToValues);

    }

}
