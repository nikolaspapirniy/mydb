package network.http.handlers;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractAjaxHandler implements HttpHandler {
    protected Map<String, Object> params = new HashMap<String, Object>();

    public void handle(HttpExchange exchange) throws IOException {
        params = (Map<String, Object>) exchange.getAttribute("parameters");

        Map<String, String> elParams = new HashMap<String, String>();
        String responseString = businessLogicProcess(elParams);

        exchange.sendResponseHeaders(200, responseString.length());
        OutputStream os = exchange.getResponseBody();
        os.write(responseString.getBytes());
        os.close();

    }

    /**
     * Processing all businesslogic of a single handler
     *
     * @param elParams expression languaes paramethers which will be used to render
     *                 page in style ${param}
     * @return response
     */
    public abstract String businessLogicProcess(Map<String, String> elParams);
}
