package network.http;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import network.http.controllers.CreateController;
import network.http.controllers.MainHandler;
import network.http.controllers.TestController;
import network.http.filters.ParameterFilter;
import network.http.handlers.MyController;
import org.apache.log4j.Logger;
import org.reflections.Reflections;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.*;

public class HttpServerCore implements Runnable {

    public static final String CONTOLLERS_PACKAGES = "network.http.controllers";
    private static Logger logger = Logger.getLogger(HttpServerCore.class);
    public static final int DEFAULT_PORT = 8080;

    private HttpServer serverInstance;
    private int port = DEFAULT_PORT;

    private List<String> controllerPackages = new ArrayList<>();

    {
        controllerPackages.add(CONTOLLERS_PACKAGES);
    }

    // Map with all binding to addresses
    private Map<String, HttpHandler> contextMap = new HashMap<String, HttpHandler>();

    public HttpServerCore() {
        super();
    }

    public HttpServerCore(int port) {
        super();
        this.port = port;
    }

    @SuppressWarnings("restriction")
    public void run() {
        try {
            serverInstance = HttpServer.create(new InetSocketAddress(port), 0);
            serverInit();
            serverInstance.start();
            logger.info("Server started!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Inits server for the start
     */
    @SuppressWarnings("restriction")
    private void serverInit() {
        serverInstance.setExecutor(null); // creates a default executor
        initMappingHandlers();
        // Init default messages
        //new DefaultJsonMessageFuller();
    }

    private void initAnnotationControllers() {
        for (String contollerPackageName : controllerPackages) {
            initAnnotationController(contollerPackageName);
        }
    }

    private void initAnnotationController(String contollersPackage) {
        Reflections reflections = new Reflections(contollersPackage);
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(MyController.class);

        for (Class<?> controller : annotated) {
            MyController myController = controller.getAnnotation(MyController.class);
            String mapping = myController.path();
            try {
                contextMap.put(mapping, (HttpHandler) controller.newInstance());
            } catch (InstantiationException e) {
                throw new RuntimeException("Can't instantiate controller: " + controller.toString(), e);
            } catch (ClassCastException e) {
                throw new RuntimeException("Controller: " + controller.toString() + " is not HttpHandler", e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException("Controller: " + controller.toString() + " is not accessible", e);
            }
        }
    }

    // Initializes handlers for addresses
    @SuppressWarnings("restriction")
    private void initMappingHandlers() {
        initAnnotationControllers();
        //customMappingInit();
        for (Map.Entry<String, HttpHandler> entry : contextMap.entrySet()) {
            HttpContext createContext = serverInstance.createContext(entry.getKey(), entry.getValue());
            createContext.getFilters().add(new ParameterFilter());
            logger.info("Added mapping for " + entry.getKey());
        }
    }

    /**
     * Initializes default mapping
     */
    private void customMappingInit() {
        contextMap.put("/test", new MainHandler());
        contextMap.put("/request_ajax", new CreateController());
        contextMap.put("/test_page", new TestController());
    }
}
