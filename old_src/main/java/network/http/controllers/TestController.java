package network.http.controllers;

import network.http.handlers.AbstractHandler;
import network.http.handlers.MyController;
import org.apache.log4j.Logger;

import java.util.Map;

@MyController(path = "/test_page")
public class TestController extends AbstractHandler {

    private static Logger logger = Logger.getLogger(TestController.class);

    @Override
    public String businessLogicProcess(Map<String, String> elParams) {
        logger.info("Test controller test_page.html called with params: " + params);
        return "test_page.html";
    }
}
