package network.http.controllers;

import core.operations.Create;
import core.request.RequestHandler;
import network.http.handlers.AbstractAjaxHandler;
import network.http.handlers.MyController;
import org.apache.log4j.Logger;

import java.util.Map;

@MyController(path = "/request_ajax")
public class CreateController extends AbstractAjaxHandler {

    private static RequestHandler requestHandler = new RequestHandler();
    private static Logger logger = Logger.getLogger(CreateController.class);
    private static Create createModel = new Create();

    @Override
    public String businessLogicProcess(Map<String, String> elParams) {
        logger.info("Request to CreateController with params" + params);

        String result = requestHandler.handle(params);
        System.out.println(params);
        return result;
    }

}
