package network.http.controllers;

import com.sun.net.httpserver.HttpHandler;
import network.http.handlers.AbstractHandler;
import network.http.handlers.MyController;
import org.apache.log4j.Logger;

import java.util.Map;

@MyController(path = "/test")
public class MainHandler extends AbstractHandler implements HttpHandler {

    private static Logger logger = Logger.getLogger(MainHandler.class);

    @Override
    public String businessLogicProcess(Map<String, String> elParams) {
        elParams.put("", "");
        return "temp.html";
    }

}
