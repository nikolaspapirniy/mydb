package core.request;

public enum RequestType {
	CREATE, 
	RETRIEVE, 
	UPDATE, 
	DELETE, 
	RETRIEVE_ALL,
	COMMIT,
	REVISION_NUMBER,
	SNAPSHOT;
}