package core.request;

import core.operations.*;
import core.response.ResponseErrorMessage;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;

import java.util.Map;

public class RequestHandler {

    /**
     * Handles request and makes operation
     *
     * @param map with params
     * @return result of execution string
     */
    public String handle(Map map) {
        JSONObject obj = new JSONObject();
        obj.putAll(map);
        return handle(obj.toString());
    }

    /**
     * Handles request and makes operation
     *
     * @param string request string
     * @return result of execution string
     */
    public String handle(String string) {
        if (string == null) {
            return ResponseErrorMessage.JSON_ERROR_INVALID_FORMAT.getMessageStringInJSON();
        }

        JSONObject jObj = null;

        // Trying to convert from string to json object
        try {
            jObj = JSONObject.fromObject(string);
        } catch (JSONException e) {
            return ResponseErrorMessage.JSON_ERROR_INVALID_FORMAT.getMessageStringInJSON();
        }

        // Defining operation type - strategy
        Operation operation = defineRequestType(jObj);
        if (operation == null)
            return ResponseErrorMessage.JSON_ERROR_NO_REQUEST_TYPE.getMessageStringInJSON();

        // Making operations staff
        String result = operation.processOperation(jObj);
        return result;
    }

    /**
     * Defines request type
     *
     * @param json
     * @return {@code RequestType} object or null if requestType undefined
     */
    private Operation defineRequestType(JSONObject json) {
        if (json.containsKey("requestType") == false)
            return null;

        String key = json.getString("requestType").toUpperCase();
        if ("CREATE".equals(key))
            return new Create();
        else if ("RETRIEVE".equals(key))
            return new Retrieve();
        else if ("UPDATE".equals(key))
            return new Update();
        else if ("DELETE".equals(key))
            return new Delete();
        else if ("RETRIEVE_ALL".equals(key))
            return new RetrieveAll();
        else if ("REVISION_NUMBER".equals(key))
            return new RevisionNumber();
        else if ("COMMIT".equals(key))
            return new Commit();
        else if ("SNAPSHOT".equals(key))
            return new Snapshot();
        else if ("REVERT".equals(key))
            return new Revert();
        else if ("SNAPSHOT_LIST".equals(key))
            return new SnapshotList();
        else
            return null;
    }

}
