package core.operations;

import core.request.RequestType;
import net.sf.json.JSONObject;
import repository.HeadRepository;

public class RevisionNumber extends AbstractOperation {

    private static HeadRepository headRepository = HeadRepository.getInstance();

    public RequestType getRequestType() {
        return RequestType.REVISION_NUMBER;
    }

    public String processOperationSpecific(JSONObject json) {
        int revision = headRepository.getWorkingRevision().getWorkingRevisionNumber();
        return "Current revision is " + revision;
    }
}
