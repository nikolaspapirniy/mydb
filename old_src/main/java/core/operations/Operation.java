package core.operations;

import net.sf.json.JSONObject;
import core.request.RequestType;

public interface Operation {

    /**
     * @return request type of the operation
     */
    RequestType getRequestType();

    /**
     * Processes the operation with specific behaviour
     *
     * @param json object with parameters
     * @return generated response
     */
    String processOperation(JsonObject json);
}
