package core.operations;

import core.request.RequestType;
import net.sf.json.JSONObject;
import repository.HeadRepository;

public class Commit extends AbstractOperation {

    private static HeadRepository headRepository = HeadRepository.getInstance();

    public RequestType getRequestType() {
        return RequestType.COMMIT;
    }

    public String processOperationSpecific(JsonObject json) {
        int before = headRepository.getWorkingRevision().getWorkingRevisionNumber();
        int revision = headRepository.commit();

        if (before == revision)
            return "No changes defined. Still on " + revision + " revision";
        else
            return "Committed successfully! Current revision number is " + revision;
    }
}
