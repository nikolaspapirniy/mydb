package core.operations;

import core.request.RequestType;
import core.response.ResponseErrorMessage;
import net.sf.json.JSONObject;
import repository.HeadRepository;

public class Retrieve extends AbstractOperation {

    private static HeadRepository headRepository = HeadRepository.getInstance();

    public RequestType getRequestType() {
        return RequestType.RETRIEVE;
    }

    public String processOperationSpecific(JsonObject json) {
        if (!json.containsKey("key"))
            return ResponseErrorMessage.RETRIEVE_ERROR_NO_KEY.getMessageStringInJSON();

        String key = json.getString("key");

        String resultString = headRepository.retrieve(key);
        if (resultString == null)
            return ResponseErrorMessage.RETRIEVE_ERROR_NO_VALUE_FOUND.getMessageStringInJSON();
        else
            return resultString;
    }
}
