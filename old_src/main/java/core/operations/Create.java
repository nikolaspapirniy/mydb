package core.operations;

import core.request.RequestType;
import core.response.ResponseErrorMessage;
import net.sf.json.JSONObject;
import repository.HeadRepository;

public class Create extends AbstractOperation {

    private static HeadRepository headRepository = HeadRepository.getInstance();

    public RequestType getRequestType() {
        return RequestType.CREATE;
    }

    public String processOperationSpecific(JsonObject json) {
        if (!json.containsKey("key"))
            return ResponseErrorMessage.CREATE_ERROR_NO_KEY.getMessageStringInJSON();
        if (!json.containsKey("value"))
            return ResponseErrorMessage.CREATE_ERROR_NO_VALUE.getMessageStringInJSON();

        String key = json.getString("key");
        String value = json.getString("value");

        boolean result = headRepository.create(key, value);
        if (result) {
            return "Value associated with " + key + " created";
        } else {
            return ResponseErrorMessage.CREATE_ERROR_RECORD_ALREADY_EXISTS.getMessageStringInJSON();
        }
    }
}
