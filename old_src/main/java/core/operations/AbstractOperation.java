package core.operations;

import net.sf.json.JSONObject;

public abstract class AbstractOperation implements Operation {

    /**
     * Operation specific action
     *
     * @param json parameters in JSON format
     * @return response message
     */
    public abstract String processOperationSpecific(JsonObject json);

    public String processOperation(JsonObject json) {
        JsonObject resultObject = new JsonObject();

        String processOperationSpecificResult = processOperationSpecific(json);
        resultObject.put("message", processOperationSpecificResult);

        return resultObject.toString();
    }
}
