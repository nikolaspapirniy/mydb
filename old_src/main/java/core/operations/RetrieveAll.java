package core.operations;

import core.request.RequestType;
import net.sf.json.JSONObject;
import repository.HeadRepository;

import java.util.Map;

public class RetrieveAll extends AbstractOperation {

    private static HeadRepository headRepository = HeadRepository.getInstance();

    public RequestType getRequestType() {
        return RequestType.RETRIEVE_ALL;
    }

    public String processOperationSpecific(JsonObject json) {

        Map<String, String> retrieveAllMap = headRepository.retrieveAll();
        if (retrieveAllMap.size() == 0) {
            return new JsonObject().toString();
        }
        JsonObject obj = new JsonObject();
        obj.putAll(retrieveAllMap);

        return obj.toString();
    }

}
