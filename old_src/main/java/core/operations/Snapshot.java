package core.operations;

import java.util.List;

import net.sf.json.JSONObject;
import repository.HeadRepository;
import core.*;
import core.request.RequestType;
import core.response.ResponseErrorMessage;

public class Snapshot extends AbstractOperation {

    private static HeadRepository headRepository = HeadRepository.getInstance();

    public RequestType getRequestType() {
	return RequestType.SNAPSHOT;
    }

    public String processOperationSpecific(JSONObject json) {
	if (!json.containsKey("snapshotNumber"))
	    return ResponseErrorMessage.SNAPSHOT_ERROR_NO_SNAPSHOT_NUMBER.getMessageStringInJSON();

	int snapshotNumber = json.getInt("snapshotNumber");
	if(headRepository.getWorkingRevision().getWorkingRevisionNumber() == snapshotNumber) {
	    return "On " + snapshotNumber + " revision already";
	}
	
	if (snapshotNumber == 0) {
	    headRepository.snapshot(snapshotNumber);
	    return "Reverted to snapshot successfully! On " + 0 + " revision now";
	}

	List<Integer> availableSnapshots = headRepository.availableSnapshots();
	if (!availableSnapshots.contains(snapshotNumber)) {
	    return ResponseErrorMessage.SNAPSHOT_ERROR_NO_SNAPSHOT_FOUND.getMessageStringInJSON();
	}

	int currentRevisionNumber = headRepository.getWorkingRevision().getWorkingRevisionNumber();

	int result = headRepository.snapshot(snapshotNumber);
	if (result == currentRevisionNumber && snapshotNumber != 0) {
	    return ResponseErrorMessage.SNAPSHOT_ERROR_NO_SNAPSHOT_FOUND.getMessageStringInJSON();
	} else {
	    return "Reverted to snapshot successfully! On " + result + " revision now";
	}
    }

}
