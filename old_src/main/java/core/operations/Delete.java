package core.operations;

import repository.HeadRepository;
import net.sf.json.JSONObject;
import core.request.RequestType;
import core.response.ResponseErrorMessage;

public class Delete extends AbstractOperation {

    private static HeadRepository headRepository = HeadRepository.getInstance();

    public RequestType getRequestType() {
	return RequestType.DELETE;
    }

    public String processOperationSpecific(JsonObject json) {
	if (!json.containsKey("key"))
	    return ResponseErrorMessage.DELETE_ERROR_NO_KEY.getMessageStringInJSON();

	String key = json.getString("key");

	String previousValue = headRepository.delete(key);
	if (previousValue == null) {
	    return ResponseErrorMessage.DELETE_ERROR_NO_RECORD_FOUND_FOR_THIS_KEY.getMessageStringInJSON();
	}
	StringBuilder sb = new StringBuilder();
	sb.append("Record deleted successfully! Previous value was ").append(previousValue);
	return sb.toString();
    }
}
