package core.operations;

import repository.HeadRepository;
import net.sf.json.JSONObject;
import core.request.RequestType;
import core.response.ResponseErrorMessage;

public class Update extends AbstractOperation {
    private static HeadRepository headRepository = HeadRepository.getInstance();

    public RequestType getRequestType() {
	return RequestType.UPDATE;
    }

    public String processOperationSpecific(JSONObject json) {
	if (!json.containsKey("key"))
	    return ResponseErrorMessage.UPDATE_ERROR_NO_KEY.getMessageStringInJSON();
	if (!json.containsKey("value"))
	    return ResponseErrorMessage.UPDATE_ERROR_NO_VALUE.getMessageStringInJSON();

	String key = json.getString("key");
	String value = json.getString("value");

	String resultString = headRepository.update(key, value);
	if (resultString == null)
	    return ResponseErrorMessage.UPDATE_ERROR_NO_VALUE_FOUND.getMessage();
	else {
	    StringBuilder sb = new StringBuilder();
	    sb.append("Record Updated successfully! Previous value associated with key was ").append(resultString);
	    return sb.toString();
	}
    }
}
