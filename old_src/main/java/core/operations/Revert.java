package core.operations;

import net.sf.json.JSONObject;
import repository.HeadRepository;
import core.request.RequestType;
import core.response.ResponseErrorMessage;

public class Revert extends AbstractOperation {

    private static HeadRepository headRepository = HeadRepository.getInstance();

    public RequestType getRequestType() {
	return RequestType.SNAPSHOT;
    }

    public String processOperationSpecific(JSONObject json) {
	// @TODO add list of available snapshots and iterate over them
	int current = headRepository.getWorkingRevision().getWorkingRevisionNumber();

	if (current == 0) {
	    return ResponseErrorMessage.REVERT_ERROR_ON_ZERO_REVISION_NOW.getMessageStringInJSON();
	} else if (current == 1) {
	    headRepository.snapshot(0);
	    return "Reverted to snapshot successfully! On 0 revision now";
	}

	else {
	    // current revision number
	    int i = headRepository.getWorkingRevision().getWorkingRevisionNumber();
	    while (i > 0) {

		int result = headRepository.snapshot(--i);
		if (result == current || result == 0) {
		    continue;
		} else {
		    return "Reverted to snapshot successfully! On " + result + " revision now";
		}
	    }
	}
	return ResponseErrorMessage.REVERT_ERROR_NO_SNAPSHOTS_AVAILABLE.getMessageStringInJSON();
    }
}
