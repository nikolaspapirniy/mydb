package core.operations;

import java.util.List;

import net.sf.json.JSONObject;
import repository.HeadRepository;
import core.*;
import core.request.RequestType;
import core.response.ResponseErrorMessage;

public class SnapshotList extends AbstractOperation{

    private static HeadRepository headRepository = HeadRepository.getInstance();

    public RequestType getRequestType() {
	return RequestType.SNAPSHOT;
    }

    public String processOperationSpecific(JSONObject json) {
	List<Integer> availableSnapshots = headRepository.availableSnapshots();
	if(availableSnapshots == null) {
	    return ResponseErrorMessage.SNAPSHOT_LIST_ERROR_NO_FOLDER.getMessageStringInJSON();
	}
	if(availableSnapshots.size() == 0) {
	    return ResponseErrorMessage.SNAPSHOT_LIST_ERROR_NO_SNAPSHOTS_AVAILABLE.getMessageStringInJSON();
	}
	
	String s = availableSnapshots.toString();
	JSONObject obj = new JSONObject();
	obj.put("available_snapshots", availableSnapshots);
	String result = obj.toString();
	return result;
    }
}
