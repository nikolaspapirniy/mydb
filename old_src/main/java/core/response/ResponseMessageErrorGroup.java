package core.response;

/**
 * Defines error group id
 *
 * @author niko
 */
public enum ResponseMessageErrorGroup {
    SYSTEM(0),
    RETRIEVING_FORMAT(1),
    CREATE(2),
    RETRIEVE(3),
    UPDATE(4),
    DELETE(5),
    RETRIEVE_ALL(6),
    SNAPSHOT(7),
    REVERT(8),
    SNAPSHOT_LIST(9);

    private Integer errorGroupId;

    private ResponseMessageErrorGroup(Integer errorGroupId) {
        this.errorGroupId = errorGroupId;
    }

    public Integer getErrorGroupId() {
        return errorGroupId;
    }

}
