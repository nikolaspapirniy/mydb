package core.response;

import net.sf.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResponseJsonMessageRepository {
    private static Map<String, JSONObject> responseMessage = new HashMap<String, JSONObject>();
    private static volatile ResponseJsonMessageRepository instance;

    private ResponseJsonMessageRepository() {

    }

    /**
     * @return instance of a class
     */
    public static ResponseJsonMessageRepository getInstance() {
        if (instance == null) {
            synchronized (ResponseJsonMessageRepository.class) {
                if (instance == null) {
                    instance = new ResponseJsonMessageRepository();
                }
            }
        }
        return instance;
    }

    /**
     * Adds json object to the map associated with message name
     *
     * @return true if there was no message before, false if message has been replaced
     */
    public boolean addMessage(String key, JSONObject object) {
        if (responseMessage.put(key, object) == null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Retrieves a message from map
     *
     * @param key
     * @return
     */
    public JSONObject getMessage(String key) {
        JSONObject jsonObject = responseMessage.get(key);
        return jsonObject;
    }
}
