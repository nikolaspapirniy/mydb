package core.response;

import net.sf.json.JSONObject;


public enum ResponseErrorMessage {
    
    // System
    JSON_ERROR_INVALID_FORMAT(
	    ResponseMessageErrorNumber.JSON_ERROR_INVALID_FORMAT_ERROR_NUMBER, 
	    ResponseMessageType.ERROR, 
	    "Format is not json valid"
	    ),
	    
    JSON_ERROR_NO_REQUEST_TYPE(
	    ResponseMessageErrorNumber.JSON_ERROR_NO_REQUEST_TYPE_ERROR_NUMBER,
	    ResponseMessageType.ERROR,
	    "There is no requestType parameter in your json string or parameter is wrong"
	    ),
	    
    // CREATE	   
    CREATE_ERROR_NO_KEY(
	    ResponseMessageErrorNumber.CREATE_ERROR_NO_KEY_ERROR_NUMBER,
	    ResponseMessageType.ERROR,
	    "There is no parameter named key."
	    ),
	    
    CREATE_ERROR_NO_VALUE(
	    ResponseMessageErrorNumber.CREATE_ERROR_NO_VALUE_ERROR_NUMBER,
	    ResponseMessageType.ERROR,
	    "There is no parameter named value."
	    ),
	    
    CREATE_ERROR_RECORD_ALREADY_EXISTS(
	    ResponseMessageErrorNumber.CREATE_ERROR_RECORD_ALREADY_EXISTS_ERROR_NUMBER,
	    ResponseMessageType.INFO,
	    "Record with this key already exists"
	    ),

    // DELETE
    DELETE_ERROR_NO_KEY(
	    ResponseMessageErrorNumber.DELETE_ERROR_NO_KEY_ERROR_NUMBER,
	    ResponseMessageType.ERROR,
	    "There is no parameter named key"
	    ),
	    
    DELETE_ERROR_NO_RECORD_FOUND_FOR_THIS_KEY(
	    ResponseMessageErrorNumber.DELETE_ERROR_NO_RECORD_FOUND_FOR_THIS_KEY_ERROR_NUMBER,
	    ResponseMessageType.INFO,
	    "No record found for this key"),
   
    // RETRIEVE	    
    RETRIEVE_ERROR_NO_KEY(
	    ResponseMessageErrorNumber.RETRIEVE_ERROR_NO_KEY_ERROR_NUMBER,
	    ResponseMessageType.ERROR,
	    "There is no parameter named key"
	    ),
	    
    RETRIEVE_ERROR_NO_VALUE_FOUND(
	    ResponseMessageErrorNumber.RETRIEVE_ERROR_NO_VALUE_FOUND_ERROR_NUMBER,
	    ResponseMessageType.INFO,
	    "No record has been found for your request key"
	    ),
	    
    // UPDATE    
    UPDATE_ERROR_NO_KEY(
	    ResponseMessageErrorNumber.UPDATE_ERROR_NO_KEY_ERROR_NUMBER,
	    ResponseMessageType.ERROR,
	    "There is no parameter named key"
	    ),
	    
    UPDATE_ERROR_NO_VALUE(
	    ResponseMessageErrorNumber.UPDATE_ERROR_NO_VALUE_ERROR_NUMBER,
	    ResponseMessageType.ERROR,
	    "There is no parameter named value"
	    ),
	    
    UPDATE_ERROR_NO_VALUE_FOUND(
	    ResponseMessageErrorNumber.UPDATE_ERROR_NO_VALUE_FOUND_ERROR_NUMBER,
	    ResponseMessageType.INFO,
	    "No value found for your key"
	    ),
	    
    // RETRIEVE ALL RECORDS
    RETRIEVE_ALL_NO_RECORDS(
	    ResponseMessageErrorNumber.RETRIEVE_ALL_NO_RECORDS_ERROR_NUMBER,
	    ResponseMessageType.INFO,
	    "No records found in current revision"
	    ),
	    
    // SNAPSHOT
    SNAPSHOT_ERROR_NO_SNAPSHOT_NUMBER(
	    ResponseMessageErrorNumber.SNAPSHOT_ERROR_NO_SNAPSHOT_NUMBER_ERROR_NUMBER,
	    ResponseMessageType.ERROR,
	    "No paramerer snapshotNumer found"
	    ),
    
    SNAPSHOT_ERROR_NO_SNAPSHOT_FOUND(
	    ResponseMessageErrorNumber.SNAPSHOT_ERROR_NO_SNAPSHOT_FOUND_ERROR_NUMBER,
	    ResponseMessageType.ERROR,
	    "No snapshot found with this number"
	    ),
    
    // REVERT TO SNAPSHOT
    REVERT_ERROR_NO_SNAPSHOTS_AVAILABLE(
	    ResponseMessageErrorNumber.REVERT_ERROR_NO_SNAPSHOTS_AVAILABLE,
	    ResponseMessageType.INFO,
	    "No snapshots available before current except 0"
	    ),
	    
    REVERT_ERROR_ON_ZERO_REVISION_NOW(
	    ResponseMessageErrorNumber.REVERT_ERROR_ON_ZERO_REVISION_NOW_ERROR_NUMBER,
	    ResponseMessageType.INFO,
	    "Can't revert because on the 0 revision now"
	    ),
    
    // SNAPSHOT LIST
    SNAPSHOT_LIST_ERROR_NO_FOLDER(
	    ResponseMessageErrorNumber.SNAPSHOT_LIST_ERROR_NO_FOLDER_ERROR_NUMBER,
	    ResponseMessageType.FATAL,
	    "No system folder. You need to reboot server to renew default settings"
	    ),
	    
    SNAPSHOT_LIST_ERROR_NO_SNAPSHOTS_AVAILABLE(
	    ResponseMessageErrorNumber.SNAPSHOT_LIST_ERROR_NO_SNAPSHOTS_AVAILABLE_ERROR_NUMBER,
	    ResponseMessageType.INFO,
	    "No snapshots available except 0"
	    );

    /**
     * DBERR
     */
    private ResponseMessageErrorNumber errorNumber;
    
    /**
     * Message of the error
     */
    private String message;
    
    /**
     * The type of the message (e.g. error / info / warning). Default - info
     */
    private ResponseMessageType type;

    private ResponseErrorMessage(String message) {
	this.message = message;
    }
    
    private ResponseErrorMessage(ResponseMessageErrorNumber errorNumber, String message) {
	this(message);
	this.errorNumber = errorNumber;
    }
 
    private ResponseErrorMessage(ResponseMessageErrorNumber errorNumber, ResponseMessageType type, String message) {
	this(errorNumber, message);
	this.type = type;
    }

    public String getMessage() {
	return message;
    }
    
    public JSONObject getMessageInJSON() {
	JSONObject object = new JSONObject();
	if(type != null)
	    object.put("type", type.toString());
	if(errorNumber != null)
	    object.put("errorNumber", errorNumber.getErrorMessage());
	if(message != null)
	    object.put("message", message);
	
	return object;
    }
    
    public String getMessageStringInJSON() {
	JSONObject object = getMessageInJSON();
	return object.toString();
    }
    
}
