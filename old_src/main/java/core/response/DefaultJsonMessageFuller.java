package core.response;

import net.sf.json.JSONObject;

/**
 * Fills messages with default values
 *
 * @author npapirniy
 */
public class DefaultJsonMessageFuller {

    public DefaultJsonMessageFuller() {
        ResponseJsonMessageRepository instance = ResponseJsonMessageRepository.getInstance();

        instance.addMessage("JSON_ERROR_INVALID_FORMAT", fill_JSON_ERROR_INVALID_FORMAT());
        instance.addMessage("JSON_ERROR_NO_REQUEST_TYPE", fill_JSON_ERROR_NO_REQUEST_TYPE());
        instance.addMessage("CREATE_ERROR_NO_KEY", fill_CREATE_ERROR_NO_KEY());
    }

    private JSONObject fill_JSON_ERROR_INVALID_FORMAT() {
        JSONObject tempObject = new JSONObject();
        tempObject.put("status", "error");
        tempObject.put("errorNumber", "DBERR-000");
        tempObject.put("errorMessage", "Your format is not json valid");
        return tempObject;
    }

    private JSONObject fill_JSON_ERROR_NO_REQUEST_TYPE() {
        JSONObject tempObject = new JSONObject();
        tempObject.put("status", "error");
        tempObject.put("errorNumber", "DBERR-001");
        tempObject.put("errorMessage", "There is no requestType parameter in your json string or parameter is wrong");
        return tempObject;
    }

    private JSONObject fill_CREATE_ERROR_NO_KEY() {
        JSONObject tempObject = new JSONObject();
        tempObject.put("status", "error");
        tempObject.put("errorNumber", "DBERR-002");
        tempObject.put("errorMessage", "Error during creating new record. There is no parameter named key.");
        return tempObject;
    }
}
