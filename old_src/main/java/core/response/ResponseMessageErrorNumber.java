package core.response;

/**
 * Error messages for errors
 *
 * @author Nikolas Papirniy
 */
public enum ResponseMessageErrorNumber {

    JSON_ERROR_INVALID_FORMAT_ERROR_NUMBER(ResponseMessageErrorGroup.SYSTEM, 1),
    JSON_ERROR_NO_REQUEST_TYPE_ERROR_NUMBER(ResponseMessageErrorGroup.SYSTEM, 2),

    CREATE_ERROR_NO_KEY_ERROR_NUMBER(ResponseMessageErrorGroup.CREATE, 1),
    CREATE_ERROR_NO_VALUE_ERROR_NUMBER(ResponseMessageErrorGroup.CREATE, 2),
    CREATE_ERROR_RECORD_ALREADY_EXISTS_ERROR_NUMBER(ResponseMessageErrorGroup.CREATE, 3),

    DELETE_ERROR_NO_KEY_ERROR_NUMBER(ResponseMessageErrorGroup.DELETE, 1),
    DELETE_ERROR_NO_RECORD_FOUND_FOR_THIS_KEY_ERROR_NUMBER(ResponseMessageErrorGroup.DELETE, 2),

    RETRIEVE_ERROR_NO_KEY_ERROR_NUMBER(ResponseMessageErrorGroup.RETRIEVE, 1),
    RETRIEVE_ERROR_NO_VALUE_FOUND_ERROR_NUMBER(ResponseMessageErrorGroup.RETRIEVE, 2),

    UPDATE_ERROR_NO_KEY_ERROR_NUMBER(ResponseMessageErrorGroup.UPDATE, 1),
    UPDATE_ERROR_NO_VALUE_ERROR_NUMBER(ResponseMessageErrorGroup.UPDATE, 2),
    UPDATE_ERROR_NO_VALUE_FOUND_ERROR_NUMBER(ResponseMessageErrorGroup.UPDATE, 3),

    RETRIEVE_ALL_NO_RECORDS_ERROR_NUMBER(ResponseMessageErrorGroup.RETRIEVE_ALL, 1),

    SNAPSHOT_ERROR_NO_SNAPSHOT_NUMBER_ERROR_NUMBER(ResponseMessageErrorGroup.SNAPSHOT, 1),
    SNAPSHOT_ERROR_NO_SNAPSHOT_FOUND_ERROR_NUMBER(ResponseMessageErrorGroup.SNAPSHOT, 2),

    REVERT_ERROR_NO_SNAPSHOTS_AVAILABLE(ResponseMessageErrorGroup.REVERT, 1),
    REVERT_ERROR_ON_ZERO_REVISION_NOW_ERROR_NUMBER(ResponseMessageErrorGroup.REVERT, 2),

    SNAPSHOT_LIST_ERROR_NO_FOLDER_ERROR_NUMBER(ResponseMessageErrorGroup.SNAPSHOT_LIST, 1),
    SNAPSHOT_LIST_ERROR_NO_SNAPSHOTS_AVAILABLE_ERROR_NUMBER(ResponseMessageErrorGroup.SNAPSHOT_LIST, 2);

    /**
     * How to format error message
     */
    private static String errorPrefix = "DBERR";
    private static String errorDeliminer = "-";

    /**
     * Error group id
     */
    private ResponseMessageErrorGroup errorGroupId;
    /**
     * Number of error
     */
    private Integer errorNumber;

    private ResponseMessageErrorNumber(ResponseMessageErrorGroup errorGroupId, Integer errorNumber) {
        this.errorGroupId = errorGroupId;
        this.errorNumber = errorNumber;
    }

    /**
     * @return error message in form "errorPrefix + errorDeliminer + errorGroupId + errorNumber"
     */
    public String getErrorMessage() {
        return errorPrefix + errorDeliminer + errorGroupId.getErrorGroupId() + errorNumber;
    }

    /**
     * @return error number for the error
     */
    public Integer getErrorNumber() {
        return errorNumber;
    }

}
