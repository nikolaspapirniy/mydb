package core.response;

public enum ResponseMessageType {

    WARNING() {
        @Override
        public String toString() {
            return "warning";
        }
    },

    INFO() {
        @Override
        public String toString() {
            return "info";
        }
    },

    ERROR() {
        @Override
        public String toString() {
            return "error";
        }
    },

    FATAL() {
        @Override
        public String toString() {
            return "fatal";
        }
    };

}
