package repository.old.static_content;

import org.apache.log4j.Logger;
import repository.HeadRepository;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Class for managing static content like txt, html etc
 *
 * @author npapirniy
 */
public class StaticContentManager {

    private static Logger logger = Logger.getLogger(StaticContentManager.class);


    private List<String> classPath = new LinkedList<String>();
    private static StaticContentManager instance;
    /**
     * Entities map
     */
    private Map<String, StaticTextEntity> textEntitiesMap = new HashMap<String, StaticTextEntity>();

    private StaticContentManager() {
        fillClassPath();
    }

    // Fills folders list to look for file there
    private void fillClassPath() {
        // @TODO - optimize ?
        classPath.add(HeadRepository.STATIC_FILES_FOLDER_NAME + File.separator);
    }


    public static StaticContentManager getInstance() {
        if (instance == null) {
            synchronized (StaticContentManager.class) {
                if (instance == null) {
                    instance = new StaticContentManager();
                }
            }
        }
        return instance;
    }

    /**
     * Adds static file to manager
     *
     * @return true if file has been added, false if IOException happened
     */
    public boolean addTextEntityToManager(String fileName, String filePath) {
        try {
            StaticTextEntity staticTextEntity = new StaticTextEntity(this, fileName, filePath);
            synchronized (textEntitiesMap) {
                textEntitiesMap.put(fileName, staticTextEntity);
            }
            logger.debug("Added file: " + fileName + " with path: " + filePath + " to manager");
            return true;
        } catch (IOException e) {
            logger.error("Couldn't add " + filePath + " to manager via IOException");
            return false;
        }
    }

    /**
     * Retrieves content of the static entity
     *
     * @param fileName of the entity
     * @return latest version of content if it exists or null if there is no
     * entity with this name
     */
    public String getTextEntityFromManager(String fileName) {
        // @TODO what if file deleted???
        synchronized (textEntitiesMap) {
            StaticTextEntity staticTextEntity = textEntitiesMap.get(fileName);
            if (staticTextEntity == null) {
                if (!tryToLoadFile(fileName)) {
                    // Still no file
                    return null;
                } else {
                    // File just appeared
                    return staticTextEntity.getContent();
                }
            } else {
                try {
                    staticTextEntity.checkForUpload();
                } catch (FileNotFoundException e) {
                    // If file has been deleted
                    logger.error("FILE " + fileName + " HAS BEEN DELETED ERROR");
                    return "FILE " + fileName + " HAS BEEN DELETED ERROR";
                }
                return staticTextEntity.getContent();
            }
        }
    }

    // Tries to load file 
    private boolean tryToLoadFile(String fileName) {
        for (String filePath : classPath) {
            if (new File(filePath + fileName).exists()) {
                addTextEntityToManager(fileName, filePath + fileName);
                return true;
            }
        }
        return false;
    }

}
