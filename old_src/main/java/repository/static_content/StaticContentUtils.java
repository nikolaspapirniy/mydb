package repository.old.static_content;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.log4j.Logger;

/**
 * Class for utility methods for static content
 * 
 * @author npapirniy
 * 
 */
public class StaticContentUtils {

    private static Logger logger = Logger.getLogger(StaticContentUtils.class);

    /**
     * Reads static content from file
     * 
     * @param filename
     *            to read static from
     * @return file in a string format, separated by line separator
     * @throws IOException
     */
    public static String readStaticFromFile(String filename) throws IOException {
	BufferedReader reader = null;
	try {
	    File f = new File(filename);
	    StringBuilder builder = new StringBuilder();
	    reader = new BufferedReader(new FileReader(f));
	    String buff = null;
	    while ((buff = reader.readLine()) != null) {
		builder.append(buff).append(System.getProperty("line.separator"));
	    }
	    return builder.toString();
	} catch (IOException e) {
	    throw new IOException();
	} finally {
	    try {
		if (reader != null) {
		    reader.close();
		}
	    } catch (IOException e) {
		logger.error("Couldn't close file deader: " + filename);
	    }
	}
    }
}
