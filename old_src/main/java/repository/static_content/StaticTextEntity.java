package repository.old.static_content;

import org.apache.log4j.Logger;
import repository.HeadRepository;
import repository.SystemConfig;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;

/**
 * Class represents static entity in memory
 *
 * @author npapirniy
 */
public class StaticTextEntity {

    private static Logger logger = Logger.getLogger(StaticTextEntity.class);

    private SystemConfig systemConfig = HeadRepository.getInstance().getSystemConfig();

    private String fileName;
    private String filePath;
    private String content;
    private long lastUploadedDate;
    private long lastModifiedTimeStamp;
    private StaticContentManager manager;

    public StaticTextEntity(StaticContentManager manager, String fileName, String filePath) throws IOException {
        this.manager = manager;
        this.fileName = fileName;
        this.filePath = filePath;
        content = StaticContentUtils.readStaticFromFile(filePath);
        lastUploadedDate = Calendar.getInstance().getTimeInMillis();
        lastModifiedTimeStamp = defineLastModifiedTimeStamp();
    }

    /**
     * Checks whether file has been changed
     *
     * @return true if file has been changed and reuploaded again, false if file
     * still the same
     * @throws FileNotFoundException if file has been deleted or corrupted
     */
    public boolean checkForUpload() throws FileNotFoundException {
        // If file deleted or corrupted
        if (checkIfFileExists() == false) {
            throw new FileNotFoundException();
        }

        // If delay time has not passed
        Integer defaultReuploadDelay = Integer.valueOf(systemConfig.getSystemConfigMap().get("defaultReuploadDelay"));
        long timeInMillis = Calendar.getInstance().getTimeInMillis();
        if ((timeInMillis - lastUploadedDate) < defaultReuploadDelay * 1000) {
            return false;
        }

        // Delay time passed we can reupload file
        if (defineLastModifiedTimeStamp() != lastModifiedTimeStamp) {
            try {
                content = StaticContentUtils.readStaticFromFile(filePath);
                lastUploadedDate = Calendar.getInstance().getTimeInMillis();
                lastModifiedTimeStamp = defineLastModifiedTimeStamp();
                logger.info("Reuploaded file: " + filePath);
                return true;
            } catch (IOException e) {
                logger.error("Error during update: " + filePath);
                throw new FileNotFoundException();
            }
        }
        return false;
    }

    // Checks whether file exists or not
    private boolean checkIfFileExists() {
        File f = new File(filePath);

        return (f.exists() && f.isFile());
    }

    // Defines lastModifiedTimeStamp
    private long defineLastModifiedTimeStamp() {
        File f = new File(filePath);
        return f.lastModified();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public long getLastUploadedDate() {
        return lastUploadedDate;
    }

    public void setLastUploadedDate(long lastUploadedDate) {
        this.lastUploadedDate = lastUploadedDate;
    }

    public long getLastModifiedTimeStamp() {
        return lastModifiedTimeStamp;
    }

    public void setLastModifiedTimeStamp(long lastModifiedTimeStamp) {
        this.lastModifiedTimeStamp = lastModifiedTimeStamp;
    }

    public StaticContentManager getManager() {
        return manager;
    }

    public void setManager(StaticContentManager manager) {
        this.manager = manager;
    }

}
