package main;

import network.http.HttpServerCore;
import org.apache.log4j.Logger;

import java.net.InetAddress;

public class MyDB {
    private static Logger logger = Logger.getLogger(MyDB.class);

    public static void start() throws Exception {
        start(HttpServerCore.DEFAULT_PORT);
    }

    public static void start(int port) throws Exception {
        new Thread(new HttpServerCore(port)).start();
        logger.info("MyDB started on host: " + InetAddress.getLocalHost().getCanonicalHostName() + ":" + port);
    }
}
