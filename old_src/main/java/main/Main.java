package main;

import network.http.HttpServerCore;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws Exception {
        MyDB.start();
    }
}
