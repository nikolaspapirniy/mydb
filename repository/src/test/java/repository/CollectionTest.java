package repository;

import com.google.gson.JsonObject;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CollectionTest {
    private Collection collection;

    @Before
    public void setUp() throws Exception {
        collection = new Collection(true);
    }

    @Test
    public void should_store_data() throws Exception {
        // given
        JsonObject data = new JsonObject();
        String givenData = "Hello";
        data.addProperty("data", givenData);

        // when
        long id = collection.insert(data);

        // then
        JsonObject result = collection.retrieve(id);
        String resultData = result.get("data").getAsString();
        assertEquals(givenData, resultData);
    }

    @Test
    public void should_delete_by_id_record() throws Exception {
        // given
        JsonObject data = new JsonObject();
        data.addProperty("data", "data_value");
        long id = collection.insert(data);

        // when
        collection.delete(id);

        // then
        assertNull(collection.retrieve(id));
    }

    @Test
    public void should_update_record() throws Exception {
        // given
        JsonObject data = new JsonObject();
        data.addProperty("data", "my_data");
        long id = collection.insert(data);

        JsonObject newData = new JsonObject();
        String my_new_data = "my_new_data";
        newData.addProperty("data", my_new_data);

        // when
        collection.update(id, newData);

        // then
        JsonObject retrievedData = collection.retrieve(id);
        assertEquals(my_new_data, retrievedData.get("data").getAsString());
    }

    @Test
    public void should_retrieve_all_records() throws Exception {
        // given
        JsonObject data1 = new JsonObject();
        data1.addProperty("data", "my_data1");
        long id1 = collection.insert(data1);

        JsonObject data2 = new JsonObject();
        data2.addProperty("data", "my_data2");
        long id2 = collection.insert(data2);

        // when
        List<JsonObject> allObjects = collection.retrieveAll();

        // then
        assertTrue(allObjects.contains(data1));
        assertTrue(allObjects.contains(data2));

        for (JsonObject obj : allObjects)
            assertTrue(obj != data1 && obj != data2);

        Assert.assertEquals(2, allObjects.size());
    }
}
