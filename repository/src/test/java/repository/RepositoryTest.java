package repository;

import com.google.gson.JsonObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertFalse;

public class RepositoryTest {
    private Repository repository;

    @Before
    public void setUp() throws Exception {
        repository = RepositoryHolder.getRepository();
    }

    @After
    public void tearDown() throws Exception {
        RepositoryHolder.getRepository().removeAllCollections();
    }

    @Test
    public void should_create_collections() throws Exception {
        // given
        String collectionName = "should_create_collections";

        // when
        Collection collection = repository.createCollection(collectionName);

        // then
        assertEquals(collection, repository.getCollection(collectionName));
    }

    @Test
    public void should_delete_collection() throws Exception {
        // given
        String collectionName = "new_collection";
        Collection collection = repository.createCollection(collectionName);

        // when
        repository.removeCollection(collectionName);

        // then
        assertNull(repository.getCollection(collectionName));
    }

    @Test
    public void should_write_data_to_file() throws Exception {
        // given
        String collectionName = "my_collection";
        JsonObject data = new JsonObject();
        data.addProperty("data1", "my_data1");
        data.addProperty("data2", "my_data2");
        Collection collection = repository.createCollection(collectionName);
        collection.insert(data);

        // when
        repository.storeToFs(collectionName);

        // then
        Collection retrievedCollection = repository.readFromFs(collectionName);
        assertEquals(collection, retrievedCollection);
    }

    @Test
    public void should_remove_all_collections() throws Exception {
        // given
        Collection collection1 = repository.createCollection("collection1");
        Collection collection2 = repository.createCollection("collection2");

        // when
        repository.removeAllCollections();

        // then
        assertFalse(repository.hasCollection(collection1.getName()));
        assertFalse(repository.hasCollection(collection2.getName()));
    }
}
