package repository;

import com.google.gson.JsonObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import repository.index.IndexDataType;
import repository.index.IndexInfo;
import repository.index.IndexType;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class IndexTest {
    public static final String COLLECTION_NAME = "my_collection";
    private Collection myCollection;

    @Before
    public void setUp() throws Exception {
        myCollection = RepositoryHolder.getRepository().createCollection(COLLECTION_NAME);
    }

    @After
    public void tearDown() throws Exception {
        RepositoryHolder.getRepository().removeAllCollections();
    }

    @Test
    public void sould_index_all_data_when_create_index() throws Exception {
        // given
        String my_field1 = "my_field1";
        String my_field2 = "my_field2";

        JsonObject object1 = new JsonObject();
        long containsValue1 = 1;
        object1.addProperty(my_field1, containsValue1);
        object1.addProperty(my_field2, 3);
        myCollection.insert(object1);

        JsonObject object2 = new JsonObject();
        long containsValue2 = 2;
        object2.addProperty(my_field1, containsValue2);
        object2.addProperty(my_field2, 4);
        myCollection.insert(object2);
        IndexInfo indexInfo = new IndexInfo(my_field1, IndexType.HASH_INDEX, IndexDataType.LONG);

        // when
        myCollection.createIndex(indexInfo);

        // then
        assertTrue(myCollection.containsInIndex(my_field1, containsValue1));
        assertTrue(myCollection.containsInIndex(my_field1, containsValue1));
    }

    @Test
    public void should_update_index_after_insert() throws Exception {
        // given
        String fieldName = "field";
        long value = 10L;
        IndexInfo indexInfo = new IndexInfo(fieldName, IndexType.HASH_INDEX, IndexDataType.LONG);
        myCollection.createIndex(indexInfo);
        JsonObject object1 = new JsonObject();
        object1.addProperty(fieldName, value);

        // when
        myCollection.insert(object1);

        // then
        assertTrue(myCollection.containsInIndex(fieldName, value));
    }

    @Test
    public void should_remove_from_index_after_delete() throws Exception {
        // given
        String fieldName = "field";
        long value = 10L;
        IndexInfo indexInfo = new IndexInfo(fieldName, IndexType.HASH_INDEX, IndexDataType.LONG);
        myCollection.createIndex(indexInfo);
        JsonObject object1 = new JsonObject();
        object1.addProperty(fieldName, value);
        long id = myCollection.insert(object1);

        // when
        myCollection.delete(id);

        // then
        assertFalse(myCollection.containsInIndex(Collection.ID_PROPERTY_NAME, id));
    }

    @Test
    public void should_update_index_when_update_data() throws Exception {
        // given
        String fieldName1 = "field1";
        String fieldName2 = "field2";
        long value1 = 10L;
        long value2 = 20L;
        IndexInfo indexInfo1 = new IndexInfo(fieldName1, IndexType.HASH_INDEX, IndexDataType.LONG);
        IndexInfo indexInfo2 = new IndexInfo(fieldName2, IndexType.HASH_INDEX, IndexDataType.LONG);
        myCollection.createIndex(indexInfo1);
        myCollection.createIndex(indexInfo2);

        JsonObject object1 = new JsonObject();
        object1.addProperty(fieldName1, value1);
        object1.addProperty(fieldName2, value2);
        long id = myCollection.insert(object1);

        // when
        JsonObject newObject = new JsonObject();
        long newValue1 = 100500L;
        long newValue2 = 100500L;
        newObject.addProperty(fieldName1, newValue1);
        newObject.addProperty(fieldName2, newValue2);

        myCollection.update(id, newObject);

        // then
        assertFalse(myCollection.containsInIndex(fieldName1, value1));
        assertFalse(myCollection.containsInIndex(fieldName2, value2));

        assertTrue(myCollection.containsInIndex(fieldName1, newValue1));
        assertTrue(myCollection.containsInIndex(fieldName2, newValue2));
    }
}
