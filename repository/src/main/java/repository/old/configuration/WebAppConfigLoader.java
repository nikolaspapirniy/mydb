package repository.old.configuration;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

public class WebAppConfigLoader {

    public void parseFile(String fileName) {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            DefaultHandler handler = new DefaultHandler() {
                boolean webApp = false;
                boolean name = false;

                @Override
                public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
                    super.startElement(uri, localName, qName, attributes);
                    System.out.println("startElement " + qName);

                    if (qName.equalsIgnoreCase("WEBAPP")) {
                        webApp = true;
                    }

                    if (qName.equalsIgnoreCase("NAME")) {
                        name = true;
                    }
                }

                public void endElement(String uri, String localName, String qName) throws SAXException {
                    System.out.println("End Element :" + qName);
                }

                public void characters(char ch[], int start, int length) throws SAXException {

                    if (webApp) {
                        System.out.println("webApp : " + new String(ch, start, length));
                        webApp = false;
                    }

                    if (name) {
                        System.out.println("name : " + new String(ch, start, length));
                        name = false;
                    }

                }

            };

            saxParser.parse(fileName, handler);
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        new WebAppConfigLoader().parseFile("repo_folder\\static_content\\webapp.xml");
        System.out.println("hello");
    }
}
