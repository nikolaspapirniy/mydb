package repository.old;

import org.apache.log4j.Logger;
import repository.old.HeadRepository.RepositoryUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

/**
 * Revision with params
 *
 * @author npapirniy
 */
public class Revision implements Cloneable {
    private static Logger logger = Logger.getLogger(Revision.class);

    private int workingRevisionNumber = 0;
    private int revisionHashCode = 0;
    private String pathToFile;
    private Map<String, String> workingMap = new HashMap<String, String>();
    private SystemConfig systemConfig;

    public Revision(SystemConfig systemConfig) {
        super();
        this.systemConfig = systemConfig;
    }

    public Revision(SystemConfig systemConfig, int revisionNumber) {
        this.systemConfig = systemConfig;
        getRevisionFromFile(revisionNumber);
    }

    /**
     * Creates new record in DB
     *
     * @param key
     * @param value
     * @return true if there is no record with this name and record creted
     */
    public boolean create(String key, String value) {
        synchronized (workingMap) {
            if (workingMap.containsKey(key)) {
                return false;
            } else {
                workingMap.put(key, value);
                return true;
            }
        }
    }

    /**
     * Retrieves record
     *
     * @param key to look for
     * @return record associated with key of null if there is no record
     */
    public String retrieve(String key) {
        synchronized (workingMap) {
            return workingMap.get(key);
        }
    }

    /**
     * Updates a record
     *
     * @param key   associated with record
     * @param value to change
     * @return true if value updated ok or false if there is no record with this
     * key or
     */
    public String update(String key, String value) {
        synchronized (workingMap) {
            if (workingMap.containsKey(key)) {
                return workingMap.put(key, value);
            }
        }
        return null;
    }

    /**
     * Deletes record
     *
     * @param key to delete
     * @return deleted value or null if there is nothing to delete
     */
    public String delete(String key) {
        synchronized (workingMap) {
            if (workingMap.containsKey(key)) {
                return workingMap.remove(key);
            }
        }
        return null;
    }

    // Creates hash code based on data
    public int calculateRevisionHashCode() {
        int result = 0;
        for (Map.Entry<String, String> entry : workingMap.entrySet()) {
            result += entry.hashCode();
        }
        return result;
    }

    /**
     * Reads data from file and initialize it to a Revision Object
     *
     * @param revisionFile file with revision
     * @return Revision Object
     */
    private void getRevisionFromFile(int revisionNumber) {
        // Revision resultRevision = new Revision(systemConfig);
        if (revisionNumber == 0) {
            return;
        }
        try {
            String lr = HeadRepository.SNAPSHOT_REVISION_FILE_NAME_TEMPLATE + revisionNumber;
            File headRevisionFile = new File(lr);
            // if file exists initialize it
            if (headRevisionFile.exists()) {
                Map<String, String> resulParamsFromFile = RepositoryUtils.readParamsFromFile(headRevisionFile);
                if (resulParamsFromFile != null) {
                    workingMap = resulParamsFromFile;
                    workingRevisionNumber = revisionNumber;
                    getMetaDataFromMap(resulParamsFromFile);
                }
            }
        } catch (FileNotFoundException e) {
            logger.debug("Error during reading revision from file");
        }
    }

    // Reads meta data and puts it to object's field
    private void getMetaDataFromMap(Map<String, String> map) {
        revisionHashCode = parseRevisionHashCode(map);
    }

    // Gets #revisionHashCode from revision map and removes it from map as meta
    // property
    private int parseRevisionHashCode(Map<String, String> map) {
        String buffer = null;
        Integer headRevisionNumberResult = null;
        // headRevisionNumber
        if ((buffer = map.remove("#revisionHashCode")) != null) {
            // in case it's wrong formatted not integer
            try {
                headRevisionNumberResult = Integer.valueOf(buffer);
                if (headRevisionNumberResult != null) {

                    return headRevisionNumberResult;
                }
            } catch (IllegalArgumentException e) {
                System.out.println("Can't parse : " + workingRevisionNumber);
            }
        }
        return 0;
    }

    @Override
    public Revision clone() throws CloneNotSupportedException {
        Revision revision = new Revision(systemConfig);
        revision.setWorkingRevisionNumberr(workingRevisionNumber);
        revision.setWorkingMap(workingMap);
        revision.setRevisionHashCode(revisionHashCode);
        return revision;
    }

    /**
     * Writing all system revision data to file
     *
     * @param systemFile file descriptor where to write
     */
    public Revision writeRevisionToFile() {
        try {
            Map<String, String> generatedMap = addMetaDataToMap();
            workingRevisionNumber = systemConfig.getHeadRevisionNumber() + 1;
            HeadRepository.RepositoryUtils.writeParamsToFile(new File(HeadRepository.SNAPSHOT_REVISION_FILE_NAME_TEMPLATE + (workingRevisionNumber)),
                    generatedMap);
            Revision revis = this.clone();
            systemConfig.setHeadRevisionNumber(workingRevisionNumber);
            systemConfig.setHeadRevision(revis);
            systemConfig.flushConfig();
            return revis;
        } catch (CloneNotSupportedException e) {
            return new Revision(systemConfig);
        }
    }

    // Adds additional meta data to workingMap
    private Map<String, String> addMetaDataToMap() {
        Map<String, String> generatedMap = new HashMap<String, String>(workingMap);
        revisionHashCode = calculateRevisionHashCode();
        generatedMap.put("#revisionHashCode", String.valueOf(revisionHashCode));
        return generatedMap;
    }

    public Map<String, String> getWorkingMap() {
        return workingMap;
    }

    public void setWorkingMap(Map<String, String> workingMap) {
        this.workingMap = workingMap;
    }

    public String getPathToFile() {
        return pathToFile;
    }

    public void setPathToFile(String pathToFile) {
        this.pathToFile = pathToFile;
    }

    public int getWorkingRevisionNumber() {
        return workingRevisionNumber;
    }

    public void setWorkingRevisionNumberr(int workingRevisionNumber) {
        this.workingRevisionNumber = workingRevisionNumber;
    }

    public int getRevisionHashCode() {
        return revisionHashCode;
    }

    public void setRevisionHashCode(int revisionHashCode) {
        this.revisionHashCode = revisionHashCode;
    }

}
