package repository.old;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HeadRepository {

    private static Logger logger = Logger.getLogger(HeadRepository.class);

    private static HeadRepository instance;
    public static final String REPO_FOLDER_NAME = "repo_folder";
    public static final String PATH_SEPARATOR = System.getProperty("line.separator");
    public static final String STATIC_FILES_FOLDER_NAME = REPO_FOLDER_NAME + File.separator + "static_content";
    public static final String SYSTEM_FILE_NAME = REPO_FOLDER_NAME + File.separator + "system_config.txt";
    public static final String SNAPSHOT_FILENAME_START = "snapshot";
    public static final String SNAPSHOT_REVISION_FILE_NAME_TEMPLATE = REPO_FOLDER_NAME + File.separator + SNAPSHOT_FILENAME_START;

    private Revision workingRevision;
    private SystemConfig systemConfig;

    private HeadRepository() {
        init();
        System.out.println(workingRevision.getWorkingMap());
    }

    public static HeadRepository getInstance() {
        if (instance == null) {
            synchronized (HeadRepository.class) {
                if (instance == null) {
                    instance = new HeadRepository();
                }
            }
        }
        return instance;
    }

    private void init() {
        systemConfig = new SystemConfig(this);
        workingRevision = systemConfig.getHeadRevision();
    }

    /**
     * Creates new record in DB
     *
     * @param key
     * @param value
     * @return true if record created, false if the record with this key already
     * present
     */
    public boolean create(String key, String value) {
        return workingRevision.create(key, value);
    }

    /**
     * Retrieves record
     *
     * @param key to look for
     * @return record associated with key of null if there is no record
     */
    public String retrieve(String key) {
        return workingRevision.retrieve(key);
    }

    /**
     * Updates a record
     *
     * @param key   associated with record
     * @param value to change
     * @return previous value associated with key or null if there is no record
     * associated with key
     */
    public String update(String key, String value) {
        return workingRevision.update(key, value);
    }

    /**
     * Deletes record
     *
     * @param key to delete
     * @return deleted value or null if there is nothing to delete
     */
    public String delete(String key) {
        return workingRevision.delete(key);
    }

    /**
     * @return all records in DB or an empty Map
     */
    public Map<String, String> retrieveAll() {
        synchronized (workingRevision.getWorkingMap()) {
            Map<String, String> copy = new HashMap<String, String>(workingRevision.getWorkingMap());
            return copy;
        }
    }

    /**
     * Commits state to file and return revision number
     *
     * @return
     */
    public int commit() {
        synchronized (workingRevision) {
            if (workingRevision.calculateRevisionHashCode() != workingRevision.getRevisionHashCode())
                workingRevision = workingRevision.writeRevisionToFile();
        }
        return workingRevision.getWorkingRevisionNumber();
    }

    /**
     * Makes snapshot - loads revision from file
     *
     * @param revisionNumber
     * @return revisionNumber of loaded version if it exists or current revision
     * number if it doesn't
     */
    public int snapshot(int revisionNumber) {
        // if revert to the first snapshot
        if (revisionNumber == 0) {
            workingRevision = new Revision(systemConfig);
            return 0;
        }

        Revision revision = new Revision(systemConfig, revisionNumber);
        if (revision.getWorkingRevisionNumber() == 0)
            return workingRevision.getWorkingRevisionNumber();
        else {
            workingRevision = revision;
            return workingRevision.getWorkingRevisionNumber();
        }
    }

    /**
     * Checks whether snapshot exists
     *
     * @param revisionNumber to check
     * @return true if it does or else if not
     */
    public boolean checkSnapshotExistence(int revisionNumber) {
        Revision revision = new Revision(systemConfig, revisionNumber);
        if (revision.getWorkingRevisionNumber() == 0)
            return false;
        else
            return true;
    }

    /**
     * Looking for available snapshots in fs
     *
     * @return list with snapshot numbers, empty if there is no snapshots, null if there is no folder
     */
    public List<Integer> availableSnapshots() {
        List<Integer> result = new ArrayList<Integer>();
        File folder = new File(REPO_FOLDER_NAME);
        if (!folder.exists())
            return Collections.emptyList();

        String[] list = folder.list();

        Pattern p = Pattern.compile("\\d+");
        for (int i = 0; i < list.length; ++i) {
            String str = list[i];
            File tempFile = new File(folder, str);

            if (tempFile.isDirectory()) {
                continue;
            } else {
                if (str.startsWith(SNAPSHOT_FILENAME_START)) {
                    String r = str.substring(SNAPSHOT_FILENAME_START.length());
                    Matcher m = p.matcher(r);
                    if (m.matches()) {
                        result.add(Integer.valueOf(r));
                    }
                }

            }
        }
        return result;
    }

    public static class RepositoryUtils {
        /**
         * Writes params to file
         *
         * @param file
         * @param map
         * @throws java.io.FileNotFoundException
         */
        public static boolean writeParamsToFile(File file, Map<String, String> map) {

            BufferedWriter writer = null;
            try {
                logger.debug("> Writes params to file: " + file.getCanonicalPath());

                writer = new BufferedWriter(new PrintWriter(file));
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    writer.write(entry.getKey() + "=" + entry.getValue() + PATH_SEPARATOR);
                }
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            } finally {
                if (writer != null)
                    try {
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        return false;
                    }
            }
            return true;
        }

        /**
         * Appends map to file
         *
         * @param file
         * @param map
         * @return
         */
        public static boolean appendParamsToFile(File file, Map<String, String> map) {
            BufferedWriter writer = null;
            try {
                logger.debug("> Writes params to file: " + file.getCanonicalPath());

                writer = new BufferedWriter(new FileWriter(file, true));
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    writer.write(entry.getKey() + "=" + entry.getValue() + PATH_SEPARATOR);
                }
                writer.flush();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            } finally {
                if (writer != null)
                    try {
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                        return false;
                    }
            }
            return true;
        }

        /**
         * Reads params from file
         *
         * @param file
         * @return result map
         * @throws java.io.FileNotFoundException
         */
        public static Map<String, String> readParamsFromFile(File file) throws FileNotFoundException {
            BufferedReader reader = null;
            String buffer = null;
            Map<String, String> resultMap = new HashMap<String, String>();
            try {
                logger.debug("> Reading params from file: " + file.getCanonicalPath());
                reader = new BufferedReader(new FileReader(file));
                while ((buffer = reader.readLine()) != null) {
                    String[] result = buffer.split("=");
                    if (result.length != 2)
                        continue;
                    resultMap.put(result[0], result[1]);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return resultMap;
        }
    }

    public Revision getWorkingRevision() {
        return workingRevision;
    }

    public void setWorkingRevision(Revision workingRevision) {
        this.workingRevision = workingRevision;
    }

    public SystemConfig getSystemConfig() {
        return systemConfig;
    }

    public void setSystemConfig(SystemConfig systemConfig) {
        this.systemConfig = systemConfig;
    }

}
