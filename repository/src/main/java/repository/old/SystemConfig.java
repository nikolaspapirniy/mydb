package repository.old;

import org.apache.log4j.Logger;
import repository.old.HeadRepository.RepositoryUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SystemConfig {
    private static Logger logger = Logger.getLogger(SystemConfig.class);
    private Map<String, String> systemConfigMap = new HashMap<String, String>();
    private int headRevisionNumber = 0;
    private Revision headRevision;
    private HeadRepository repository;

    public SystemConfig(HeadRepository repository) {
        super();
        this.repository = repository;
        loadOrCreateSystemConfig();
        headRevision = new Revision(this, headRevisionNumber);
        fillMapWithDefaultValues();
    }

    private void init() {

    }

    /**
     * Flushes changes to files
     */
    public void flushConfig() {
        // @TODO create hashcode for systemConfig
        writeConfigToFile(new File(HeadRepository.SYSTEM_FILE_NAME));
    }

    // Parses String to int and returns HeadRevisionNumber
    private int headRevisionNumberParse() {
        String buffer = null;
        Integer headRevisionNumberResult = null;
        // headRevisionNumber
        if ((buffer = systemConfigMap.get("headRevisionNumber")) != null) {
            // in case it's wrong formatted not integer
            try {
                headRevisionNumberResult = Integer.valueOf(buffer);
                if (headRevisionNumberResult != null) {
                    return headRevisionNumberResult;
                }
            } catch (IllegalArgumentException e) {
                System.out.println("Can't parse : " + headRevisionNumber);
            }
        }
        return 0;
    }

    private void loadOrCreateSystemConfig() {
        File systemFile = new File(HeadRepository.SYSTEM_FILE_NAME);
        File systemFolder = new File(HeadRepository.REPO_FOLDER_NAME);

        ensureSystemFolderExists(systemFolder);

        if (!systemFile.exists())
            writeConfigToFile(systemFile);
        else
            readConfigFile(systemFile);
    }

    private void ensureSystemFolderExists(File systemFolder) {
        if (!systemFolder.exists()) {
            systemFolder.mkdir();
        }
    }

    // Generates map with default values
    private void fillMapWithDefaultValues() {
        systemConfigMap.put("defaultReuploadDelay", "10");
        //systemConfigMap.put("headRevisionNumber", String.valueOf(headRevisionNumber));
        // systemConfigMap.put("createDate", new
        // SimpleDateFormat("dd-MM-YYYY hh-mm-ss").format(new Date()));
    }

    // Fills values from map
    private void fillValuesFromMap() {
        List<Integer> availableSnapshots = repository.availableSnapshots();
        if (availableSnapshots.size() == 0) {
            headRevisionNumber = 0;
        } else {
            Collections.sort(availableSnapshots);
            Integer integer = availableSnapshots.get(availableSnapshots.size() - 1);
            headRevisionNumber = integer;
            //headRevisionNumberParse();
        }
    }

    // Fills map with values
    private void fillMapFromValues() {
        fillMapWithDefaultValues();
        systemConfigMap.put("headRevisionNumber", String.valueOf(headRevisionNumber));
    }

    private void writeConfigToFile(File systemFile) {
        fillMapFromValues();
        HeadRepository.RepositoryUtils.writeParamsToFile(systemFile, systemConfigMap);
    }

    private void readConfigFile(File systemFile) {
        try {
            systemConfigMap = RepositoryUtils.readParamsFromFile(systemFile);
            fillValuesFromMap();
        } catch (FileNotFoundException e) {
            throw new RuntimeException("File with system config is not found!");
        }
    }

    public Map<String, String> getSystemConfigMap() {
        return systemConfigMap;
    }

    public void setSystemConfigMap(Map<String, String> systemConfigMap) {
        this.systemConfigMap = systemConfigMap;
    }

    public int getHeadRevisionNumber() {
        return headRevisionNumber;
    }

    public void setHeadRevisionNumber(int headRevisionNumber) {
        this.headRevisionNumber = headRevisionNumber;
    }

    public Revision getHeadRevision() {
        return headRevision;
    }

    public void setHeadRevision(Revision headRevision) {
        this.headRevision = headRevision;
    }


    public static class DEFAULT_VALUES {
        public static final String DEFAULT_REUPLOAD_DELAY = "10";

        public void fillMapWithDefaultValues(Map<String, String> systemConfigMap) {

        }
    }
}
