package repository.index;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

public class HashIndex {
    private Map<Object, JsonObject> index = new ConcurrentHashMap<Object, JsonObject>();
    private IndexInfo indexInfo;
    private List<JsonObject> data;
    private transient ReentrantLock indexLock;
    private boolean isIndexed;
    private volatile int modCount;

    public HashIndex(List<JsonObject> data, IndexInfo indexInfo) {
        this.indexInfo = indexInfo;
        this.data = data;
        indexLock = new ReentrantLock();
    }

    public boolean isIndexed() {
        return isIndexed;
    }

    public JsonObject getData(Object key) {
        return index.get(key);
    }

    public void indexByField() {
        indexLock.lock();
        for (JsonObject jsonObject : data)
            indexSingleElement(jsonObject);
        isIndexed = true;
        indexLock.unlock();
    }

    public void addToIndex(JsonObject objectToIndex) {
        indexLock.lock();
        indexSingleElement(objectToIndex);
        indexLock.unlock();
    }

    public void removeFromIndex(JsonObject objectToRemoveFromIndex) {
        Object fieldFromObject = getFieldFromObject(objectToRemoveFromIndex, indexInfo.fieldName, indexInfo.indexDataType);
        if (fieldFromObject == null)
            return;
        else
            removeRecordFromIndex(fieldFromObject);
    }

    private void removeRecordFromIndex(Object fieldFromObject) {
        indexLock.lock();
        index.remove(fieldFromObject);
        modCount++;
        indexLock.unlock();
    }

    public boolean containsInIndex(Object value) {
        return index.containsKey(value);
    }

    public void updateIndex(JsonObject oldObject, JsonObject newData) {
        indexLock.lock();
        Object oldFieldValue = getFieldFromObject(oldObject, indexInfo.fieldName, indexInfo.indexDataType);
        Object newFieldValue = getFieldFromObject(newData, indexInfo.fieldName, indexInfo.indexDataType);
        index.remove(oldFieldValue);
        index.put(newFieldValue, newData);
        modCount++;
        indexLock.unlock();
    }

    private void indexSingleElement(JsonObject jsonObject) {
        Object fieldValue = getFieldFromObject(jsonObject, indexInfo.fieldName, indexInfo.indexDataType);
        if (fieldValue != null) {
            index.put(fieldValue, jsonObject);
            modCount++;
        }
    }

    private Object getFieldFromObject(JsonObject jsonObject, String fieldName, IndexDataType indexDataType) {
        JsonElement jsonElement = jsonObject.get(fieldName);
        if (jsonElement == null)
            return null;

        switch (indexDataType) {
            case INT:
                return jsonElement.getAsInt();
            case STRING:
                return jsonElement.getAsString();
            case LONG:
                return jsonElement.getAsLong();
            default:
                return null;
        }
    }

    Map<Object, JsonObject> getIndex() {
        return index;
    }

    void setIndex(Map<Object, JsonObject> index) {
        this.index = index;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HashIndex index = (HashIndex) o;

        if (isIndexed != index.isIndexed) return false;
        if (modCount != index.modCount) return false;
        if (indexInfo != null ? !indexInfo.equals(index.indexInfo) : index.indexInfo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = indexInfo != null ? indexInfo.hashCode() : 0;
        result = 31 * result + (isIndexed ? 1 : 0);
        result = 31 * result + modCount;
        return result;
    }
}
