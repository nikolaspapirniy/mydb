package repository.index;

public enum IndexType {
    RB_INDEX, HASH_INDEX;
}
