package repository.index;

public class IndexInfo {
    public final String fieldName;
    public final IndexType indexType;
    public final IndexDataType indexDataType;

    public IndexInfo(String fieldName, IndexType indexType, IndexDataType indexDataType) {
        this.fieldName = fieldName;
        this.indexType = indexType;
        this.indexDataType = indexDataType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IndexInfo indexInfo = (IndexInfo) o;

        if (fieldName != null ? !fieldName.equals(indexInfo.fieldName) : indexInfo.fieldName != null) return false;
        if (indexDataType != indexInfo.indexDataType) return false;
        if (indexType != indexInfo.indexType) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = fieldName != null ? fieldName.hashCode() : 0;
        result = 31 * result + (indexType != null ? indexType.hashCode() : 0);
        result = 31 * result + (indexDataType != null ? indexDataType.hashCode() : 0);
        return result;
    }
}
