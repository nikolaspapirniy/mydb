package repository.index;

import com.google.gson.JsonObject;
import repository.Collection;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class IndexManager implements Serializable {
    private Map<String, HashIndex> indexes = new ConcurrentHashMap<String, HashIndex>();

    public void addIndex(List<JsonObject> data, IndexInfo indexInfo) {
        HashIndex index = createIndex(data, indexInfo);
        index.indexByField();
        indexes.put(indexInfo.fieldName, index);
    }

    public HashIndex createIndex(List<JsonObject> data, IndexInfo indexInfo) {
        switch (indexInfo.indexType) {
            case HASH_INDEX:
                return new HashIndex(data, indexInfo);
            default:
                throw new RuntimeException("Unknown index type");
        }
    }

    public void addObjectToIndexes(JsonObject objectToIndex) {
        for (Map.Entry<String, HashIndex> index : indexes.entrySet())
            addObjectToIndexIfHasIndexField(objectToIndex, index);
    }

    private void addObjectToIndexIfHasIndexField(JsonObject objectToIndex, Map.Entry<String, HashIndex> index) {
        if (hasFieldWithIndex(objectToIndex, index))
            index.getValue().addToIndex(objectToIndex);
    }

    private boolean hasFieldWithIndex(JsonObject objectToIndex, Map.Entry<String, HashIndex> index) {
        return objectToIndex.get(index.getKey()) != null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IndexManager that = (IndexManager) o;

        if (indexes != null ? !indexes.equals(that.indexes) : that.indexes != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return indexes != null ? indexes.hashCode() : 0;
    }

    public void removeObjectFromIndexes(JsonObject objectToRemoveFromIndex) {
        for (Map.Entry<String, HashIndex> index : indexes.entrySet()) {
            if (hasFieldWithIndex(objectToRemoveFromIndex, index)) {
                index.getValue().removeFromIndex(objectToRemoveFromIndex);
            }
        }
    }

    public void updateIndexes(long id, JsonObject newData) {
        HashIndex idIndex = indexes.get(Collection.ID_PROPERTY_NAME);
        JsonObject oldObject = idIndex.getData(id);

        for (Map.Entry<String, HashIndex> index : indexes.entrySet()) {
            if (isFieldRemoved(newData, oldObject, index)) {
                index.getValue().removeFromIndex(oldObject);
            } else if (isStillContainsField(newData, oldObject, index)) {
                index.getValue().updateIndex(oldObject, newData);
            }
        }
    }

    private boolean isFieldRemoved(JsonObject newData, JsonObject oldObject, Map.Entry<String, HashIndex> index) {
        return hasFieldWithIndex(oldObject, index) && !hasFieldWithIndex(newData, index);
    }

    private boolean isStillContainsField(JsonObject newData, JsonObject oldObject, Map.Entry<String, HashIndex> index) {
        return hasFieldWithIndex(oldObject, index) && hasFieldWithIndex(newData, index);
    }

    public boolean containsInIndex(String fieldName, Object indexValue) {
        HashIndex index = indexes.get(fieldName);
        return (index == null) ? false : index.containsInIndex(indexValue);
    }
}
