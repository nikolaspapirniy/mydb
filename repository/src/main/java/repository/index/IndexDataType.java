package repository.index;

public enum  IndexDataType {
    INT, LONG, STRING;
}
