package repository;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import repository.index.IndexDataType;
import repository.index.IndexInfo;
import repository.index.IndexManager;
import repository.index.IndexType;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Collection {
    public static final String ID_PROPERTY_NAME = "id_";
    private CopyOnWriteArrayList<JsonObject> data = new CopyOnWriteArrayList<JsonObject>();
    private transient IndexManager indexManager = new IndexManager();
    private String collectionName = "default";

    private final transient Gson gson = new Gson();
    private final transient JsonParser jsonParser = new JsonParser();

    public Collection() {
    }

    public Collection(boolean createDefaultIndex) {
        if (createDefaultIndex)
            createDefaultIndexes();
    }

    private void createDefaultIndexes() {
        IndexInfo indexInfo = new IndexInfo(Collection.ID_PROPERTY_NAME, IndexType.HASH_INDEX, IndexDataType.LONG);
        createIndex(indexInfo);
    }

    public void createIndex(IndexInfo indexInfo) {
        indexManager.addIndex(data, indexInfo);
    }

    public boolean containsInIndex(String fieldName, Object indexValue) {
        return indexManager.containsInIndex(fieldName, indexValue);
    }

    public Collection(String collectionName) {
        this(true);
        this.collectionName = collectionName;
    }

    public String getName() {
        return collectionName;
    }

    public long insert(JsonObject insertData) {
        ensureIdPropertyNotSet(insertData);

        long id = addIdToRecord(insertData);
        putObjectToStorage(insertData);
        return id;
    }

    private void putObjectToStorage(JsonObject insertData) {
        indexManager.addObjectToIndexes(insertData);
        data.add(insertData);
    }

    private long addIdToRecord(JsonObject insertData) {
        long id = generateId();
        setIdToJson(insertData, id);
        return id;
    }

    private void setIdToJson(JsonObject insertData, long id) {
        if (insertData.get(ID_PROPERTY_NAME) == null)
            insertData.addProperty(ID_PROPERTY_NAME, id);
    }

    private long generateId() {
        return System.currentTimeMillis();
    }

    private void ensureIdPropertyNotSet(JsonObject insertData) {
        if (insertData.has(ID_PROPERTY_NAME))
            throw new IllegalStateException(String.format("Field %s should not be filled!", ID_PROPERTY_NAME));
    }

    public JsonObject retrieve(long id) {
        return getObjectFromStorage(id);
    }

    private JsonObject getObjectFromStorage(long id) {
        return getFromStorage(id).result;
    }

    private ScanResult getFromStorage(long id) {
        return scan(id);
    }

    private ScanResult scan(long id) {
        for (int i = 0; i < data.size(); ++i) {
            JsonObject jsonObject = data.get(i);
            if (jsonObject.get(ID_PROPERTY_NAME).getAsLong() == id)
                return new ScanResult(i, copyJson(jsonObject));
        }
        return new ScanResult();
    }

    public void delete(long id) {
        ScanResult fromStorage = getFromStorage(id);
        if (fromStorage.result != null) {
            data.remove(fromStorage.index);
            indexManager.removeObjectFromIndexes(fromStorage.result);
        }
    }

    public void update(long id, JsonObject newData) {
        ScanResult fromStorage = getFromStorage(id);
        if (fromStorage.result == null || fromStorage.result.equals(newData))
            return;

        setIdToJson(newData, id);
        data.set(fromStorage.index, newData);
        indexManager.updateIndexes(id, newData);
    }

    IndexManager getIndexManager() {
        return indexManager;
    }

    void setIndexManager(IndexManager indexManager) {
        this.indexManager = indexManager;
    }

    public List<JsonObject> retrieveAll() {
        List<JsonObject> result = deepCopy(data);

        return result;
    }

    private List<JsonObject> deepCopy(List<JsonObject> data) {
        List<JsonObject> result = new ArrayList<JsonObject>();
        for (JsonObject obj : data)
            result.add(copyJson(obj));
        return result;
    }

    private JsonObject copyJson(JsonObject obj) {
        String json = gson.toJson(obj);
        return (JsonObject) jsonParser.parse(json);
    }

    private static class ScanResult {
        public static final int NO_INDEX_VALUE = -1;
        public final int index;
        public final JsonObject result;

        private ScanResult(int index, JsonObject result) {
            this.index = index;
            this.result = result;
        }

        private ScanResult() {
            this.index = NO_INDEX_VALUE;
            this.result = null;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Collection that = (Collection) o;

        if (collectionName != null ? !collectionName.equals(that.collectionName) : that.collectionName != null)
            return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        if (indexManager != null ? !indexManager.equals(that.indexManager) : that.indexManager != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = data != null ? data.hashCode() : 0;
        result = 31 * result + (indexManager != null ? indexManager.hashCode() : 0);
        result = 31 * result + (collectionName != null ? collectionName.hashCode() : 0);
        return result;
    }
}
