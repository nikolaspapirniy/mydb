package repository;

import com.google.gson.Gson;
import properties.AppProperties;
import properties.AppPropertyValues;
import repository.index.IndexManager;

import java.io.*;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class Repository {
    private Map<String, Collection> collections = new ConcurrentHashMap<String, Collection>();

    protected Repository() {
    }

    public Collection createCollection(String collectionName) {
        return collections.containsKey(collectionName) ? null : createCollectionThatDoesntExist(collectionName);
    }

    private Collection createCollectionThatDoesntExist(String collectionName) {
        Collection collection = new Collection(collectionName);
        collections.put(collectionName, collection);
        return collection;
    }

    public Collection getCollection(String collectionName) {
        return collections.get(collectionName);
    }

    public boolean hasCollection(String collectionName) {
        return collections.containsKey(collectionName);
    }

    public void removeCollection(String collectionName) {
        collections.remove(collectionName);
    }

    public void storeToFs(String collectionName) throws IOException {
        ensureCollectionExists(collectionName);

        String folderPath = AppProperties.getPropertyAsString(AppPropertyValues.FOLDER_PATH);
        String indexEnding = AppProperties.getPropertyAsString(AppPropertyValues.INDEX_ENDING);

        createFolderIfNotExists(folderPath);

        storeData(collectionName, folderPath);
        storeIndex(collectionName, folderPath, indexEnding);
    }

    private void storeIndex(String collectionName, String folderPath, String indexEnding) throws IOException {
        FileOutputStream indexOut = new FileOutputStream(getIndexFilePath(collectionName, folderPath, indexEnding));
        ObjectOutputStream oIndexOut = new ObjectOutputStream(indexOut);

        Gson gson = new Gson();
        String indexManagerAsJson = gson.toJson(collections.get(collectionName).getIndexManager());

        oIndexOut.writeObject(indexManagerAsJson);
        oIndexOut.close();
    }

    private String getIndexFilePath(String collectionName, String folderPath, String indexEnding) {
        return getFileStoredPath(getIndexFileName(collectionName, indexEnding), folderPath);
    }

    private String getIndexFileName(String collectionName, String indexEnding) {
        return collectionName + indexEnding;
    }

    private void storeData(String collectionName, String folderPath) throws IOException {
        FileOutputStream dataOut = new FileOutputStream(getFileStoredPath(collectionName, folderPath));
        ObjectOutputStream oDataOut = new ObjectOutputStream(dataOut);

        Gson gson = new Gson();
        String collectionAsJson = gson.toJson(collections.get(collectionName));

        oDataOut.writeObject(collectionAsJson);
        oDataOut.close();
    }

    private static void createFolderIfNotExists(String folderPath) throws IOException {
        File folder = new File(folderPath);
        if (!folder.exists())
            folder.mkdir();
    }

    private static String getFileStoredPath(String collectionName, String folderPath) {
        return folderPath + File.separator + collectionName;
    }

    private void ensureCollectionExists(String collectionName) {
        if (collections.get(collectionName) == null)
            throw new NoSuchElementException("No collection with name = " + collectionName);
    }

    public Collection readFromFs(String collectionName) throws IOException {
        String folderPath = AppProperties.getPropertyAsString(AppPropertyValues.FOLDER_PATH);

        ensureFileWithCollectionExists(collectionName, folderPath);
        Collection collection = readData(collectionName, folderPath);
        IndexManager indexManager = readIndex(collectionName, folderPath);
        collection.setIndexManager(indexManager);

        collections.put(collectionName, collection);
        return collection;
    }

    private IndexManager readIndex(String indexName, String folderPath) throws IOException {
        String indexEnding = AppProperties.getPropertyAsString(AppPropertyValues.INDEX_ENDING);

        FileInputStream fileInputStream = new FileInputStream(getIndexFilePath(indexName, folderPath, indexEnding));

        ObjectInputStream ois = new ObjectInputStream(fileInputStream);
        IndexManager indexManager = null;
        try {
            Gson gson = new Gson();
            indexManager = gson.fromJson((String) ois.readObject(), IndexManager.class);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("File with index name " + indexName + " has wrong data inside");
        }
        return indexManager;
    }

    private Collection readData(String collectionName, String folderPath) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(getFileStoredPath(collectionName, folderPath));

        ObjectInputStream ois = new ObjectInputStream(fileInputStream);
        Collection collection = null;
        try {
            Gson gson = new Gson();
            String objectAsString = (String) ois.readObject();
            collection = gson.fromJson(objectAsString, Collection.class);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("File with collection " + collectionName + " has wrong data inside");
        }
        return collection;
    }

    private static void ensureFileWithCollectionExists(String collectionName, String folderPath) {
        File f = new File(getFileStoredPath(collectionName, folderPath));
        if (!f.exists())
            throw new IllegalArgumentException("Can't load collection! No file");
    }

    public void removeAllCollections() {
        for (Map.Entry<String, Collection> element : collections.entrySet()) {
            collections.remove(element.getKey());
        }
    }
}
