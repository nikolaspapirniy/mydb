package repository;

public class RepositoryHolder {
    private static Repository repository;

    public static Repository getRepository() {
        if (repository == null) {
            synchronized (RepositoryHolder.class) {
                if (repository == null) {
                    repository = new Repository();
                }
            }
        }
        return repository;
    }
}
