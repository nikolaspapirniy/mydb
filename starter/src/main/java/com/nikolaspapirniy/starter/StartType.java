package com.nikolaspapirniy.starter;

public enum StartType {
    CONSOLE, HTTP, SOCKET;
}
