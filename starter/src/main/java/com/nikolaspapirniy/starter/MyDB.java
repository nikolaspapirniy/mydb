package com.nikolaspapirniy.starter;

import network.http.HttpServerCore;
import org.apache.log4j.Logger;

import java.net.InetAddress;

public class MyDB {
    public static final StartType DEFAULT_INTERFACE = StartType.HTTP;
    private static Logger logger = Logger.getLogger(MyDB.class);

    public static void start() throws Exception {
        start(DEFAULT_INTERFACE);
    }

    public static void start(StartType startType) throws Exception {
        switch (startType) {
            case HTTP:
                startHttp();
                break;
            default:
                logger.error("Unknown start type!");
                System.exit(0);
        }
    }

    private static void startHttp() throws Exception {
        new Thread(new HttpServerCore()).start();
    }
}
