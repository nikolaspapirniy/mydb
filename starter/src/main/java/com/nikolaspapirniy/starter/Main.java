package com.nikolaspapirniy.starter;

import org.apache.log4j.Logger;

public class Main {
    public static final String WRONG_START_PARAMETER =
            "Wrong start parameter for interface! Available parameters: http, console, socket";

    private static Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) throws Exception {
        if (noArgs(args)) {
            logger.info("Started with default interface: " + MyDB.DEFAULT_INTERFACE);
            MyDB.start();
        } else {
            MyDB.start(getStartType(args));
        }
    }

    private static StartType getStartType(String[] args) {
        try {
            return StartType.valueOf(args[0].toUpperCase());
        } catch (IllegalArgumentException e) {
            logger.error(WRONG_START_PARAMETER);
            System.exit(0);
        }
        return null;
    }

    private static boolean noArgs(String[] args) {
        return args == null || args.length == 0;
    }
}
