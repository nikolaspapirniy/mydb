import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class HashRing {
    int hash(int[] ends, int key) {
        int max = 0;
        for (int i : ends)
            if (i > max) max = i; // ищем верхнюю границу
        int h = key % max; // хешируем по границе
        for (int i = 0; i < ends.length; i++) {
            if (h < ends[i])
                return i; // ищем бакет, в который попадает hash
        }
        return 0;
    } // не нашли? - кладем в первый бакет

    // раскладываем по бакетам и красиво выводим на экран что получилось
    void dump(int[] ends, int[] probes) {
        // tree map - ключи отсортированы по возрастанию - первым будет бакет 0 ...
        Map<Integer, List<Integer>> m = new TreeMap<Integer, List<Integer>>();
        for (int key : probes) {
            int h = hash(ends, key);
            if (m.get(h) == null) m.put(h, new ArrayList<Integer>());
            m.get(h).add(key);
        }
        System.out.println(m);
    }

    static int[] ends1 = new int[]{100, 200, 300, 400};

    static int[] ends2 = new int[]{90, 200, 275, 310, 400};

    public static void main(String[] args) throws Exception {
        int[] probes = new int[]{7, 67, 127, 187, 215, 260, 285,
                290, 299, 312, 315, 325, 370, 490, 800, 825};
        new HashRing().dump(ends1, probes);
        new HashRing().dump(ends2, probes);
    }

}
