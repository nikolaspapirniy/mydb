package network.http.processors.tags;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;


public class ForEachTagProcessorTest {

    @Test
    public void should_replace_for_each_tag_with_iteration() throws Exception {
        // given
        String stringWithForEach = "<html><forEach items=\"collection\" var=\"item\">${item.param}</forEach></html>";
        Map<String, Object> params = new HashMap<>();
        params.put("collection", Arrays.asList(new ClassWithParameters("1"), new ClassWithParameters("2"), new ClassWithParameters("3")));

        // when
        String result = ForEachTagProcessor.process(stringWithForEach, params);

        // then
        assertEquals("<html>123</html>", result);
    }

    @Test(expected = ForEachTagProcessorException.class)
    public void should_throw_ex_if_items_attr_not_specified() throws Exception {
        // given
        String stringWithForEach = "<html><forEach var=\"item\">${item.param}</forEach></html>";

        // when
        String result = ForEachTagProcessor.process(stringWithForEach, new HashMap<String, Object>());
    }

    @Test(expected = ForEachTagProcessorException.class)
    public void should_throw_ex_if_var_attr_not_specified() throws Exception {
        // given
        String stringWithForEach = "<html><forEach var=\"item\">${item.param}</forEach></html>";

        // when
        String result = ForEachTagProcessor.process(stringWithForEach, new HashMap<String, Object>());
    }

    private String readFile(String fileName) throws IOException {
        URL resource = Thread.currentThread().getContextClassLoader().getResource(fileName);
        BufferedReader reader = new BufferedReader(new FileReader(resource.getFile()));
        String buf;
        StringBuilder sb = new StringBuilder();
        while ((buf = reader.readLine()) != null) {
            sb.append(buf);
            sb.append("\n");
        }
        return sb.toString();
    }

    public class ClassWithParameters {
        private String param;

        public ClassWithParameters(String param) {
            this.param = param;
        }

        public String getParam() {
            return param;
        }

        public void setParam(String param) {
            this.param = param;
        }
    }
}
