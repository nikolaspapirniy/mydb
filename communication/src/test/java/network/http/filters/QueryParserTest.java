package network.http.filters;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class QueryParserTest {
    public static final String PARAM_DELIMINER = "=";
    private Map<String, Object> parameters;

    @Before
    public void setUp() throws Exception {
        parameters = new HashMap<String, Object>();
    }

    @Test
    public void should_not_parse_any_params_if_no_query() throws Exception {
        // given
        String query = null;

        // when
        QueryParser.parseQuery(query, parameters);

        // then
        assertTrue(parameters.isEmpty());
    }

    @Test
    public void should_parse_one_parameter() throws Exception {
        // given
        String key = "key";
        String value = "value";
        String query = key + QueryParser.HTTP_PARAM_SEPARATOR + value;


        // when
        QueryParser.parseQuery(query, parameters);

        // then
        assertEquals(value, parameters.get(key));
    }

    @Test
    public void should_parse_many_parameters() throws Exception {
        // given
        String key1 = "key1", value1 = "value1";
        String key2 = "key2", value2 = "value2";
        String query = key1 + QueryParser.HTTP_PARAM_SEPARATOR + value1 + QueryParser.HTTP_PARAMS_DELIMITER + key2 + QueryParser.HTTP_PARAM_SEPARATOR + value2;

        // when
        QueryParser.parseQuery(query, parameters);

        // then
        assertEquals(value1, parameters.get(key1));
        assertEquals(value2, parameters.get(key2));
    }

    @Test
    public void should_store_several_values_with_one_key() throws Exception {
        // given
        String key = "key1", value1 = "value1", value2 = "value2";
        String query = key + QueryParser.HTTP_PARAM_SEPARATOR + value1 + QueryParser.HTTP_PARAMS_DELIMITER + key + QueryParser.HTTP_PARAM_SEPARATOR + value2;

        // when
        QueryParser.parseQuery(query, parameters);

        // then
        List result = (List) parameters.get(key);
        assertTrue(result.contains(value1));
        assertTrue(result.contains(value2));

    }
}
