//package network.socket;
//
//import core.request.OperationHandler;
//import org.apache.log4j.Logger;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.net.Socket;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//public class RequestActor implements Runnable {
//
//    private static Logger logger = Logger.getLogger(RequestActor.class);
//    private Socket socket;
//
//    public RequestActor(Socket s) {
//        socket = s;
//    }
//
//    public void run() {
//        logger.info("Actor started");
//        String date = new SimpleDateFormat("HH-mm-ss").format(new Date());
//        InputStream inputStream = null;
//        OutputStream outputStream = null;
//        try {
//            inputStream = socket.getInputStream();
//            byte[] buffer = new byte[1024];
//            int available = inputStream.available();
//
//            StringBuilder builder = new StringBuilder();
//
//            // Reading from stream
//            while ((available = inputStream.available()) != 0) {
//                logger.debug("> available: " + available);
//                if (available > buffer.length) {
//                    available = buffer.length;
//                }
//                inputStream.read(buffer, 0, available);
//
//                logger.debug("> copied : " + available);
//                builder.append(new String(buffer, 0, available));
//            }
//            logger.info("> in(" + date + ") : " + new String(builder));
//
//            // Processing request
//            OperationHandler handle = new OperationHandler();
//            String responseMessage = handle.handle(builder.toString());
//
//            // Temp mock
//            //responseMessage = "Response here!";
//
//            // Sending response
//            outputStream = socket.getOutputStream();
//            outputStream.write(responseMessage.getBytes());
//            logger.info("> out(" + date + ")" + " : " + responseMessage);
//            outputStream.flush();
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (outputStream != null) {
//                try {
//                    outputStream.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            if (inputStream != null) {
//                try {
//                    inputStream.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            if (socket != null) {
//                try {
//                    socket.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//    }
//
//}
