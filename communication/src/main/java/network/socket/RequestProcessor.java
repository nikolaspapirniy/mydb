//package network.socket;
//
//import java.io.IOException;
//import java.net.ServerSocket;
//import java.net.Socket;
//
//public class RequestProcessor implements Runnable {
//
//    private ServerSocket serverSocket;
//    private int port;
//
//    public RequestProcessor(int port) throws IOException {
//        super();
//        this.port = port;
//        serverSocket = new ServerSocket(port);
//    }
//
//    public void run() {
//        while (!Thread.interrupted()) {
//            try {
//                Socket s = serverSocket.accept();
//                new Thread(new RequestActor(s)).start();
//            } catch (IOException e) {
//                e.printStackTrace();
//                break;
//            }
//
//        }
//    }
//
//}
