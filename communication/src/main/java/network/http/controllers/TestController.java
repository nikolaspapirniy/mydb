package network.http.controllers;

import network.http.handlers.AbstractSimpleHandler;
import network.http.annotations.MyController;
import org.apache.log4j.Logger;

import java.util.Map;

@MyController(path = "/test_page")
public class TestController extends AbstractSimpleHandler {

    private static Logger logger = Logger.getLogger(TestController.class);

    @Override
    public String process(Map<String, Object> elParams) {
        logger.info("Test controller test_page.html called with params: " + params);
        return "test_page.html";
    }
}
