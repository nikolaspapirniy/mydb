package network.http.controllers;

import core.request.OperationHandler;
import network.http.handlers.AbstractAjaxHandler;
import network.http.annotations.MyController;
import org.apache.log4j.Logger;

import java.util.Map;

@MyController(path = "/request_ajax")
public class CreateController extends AbstractAjaxHandler {

    private static OperationHandler operationHandler = new OperationHandler();
    private static Logger logger = Logger.getLogger(CreateController.class);

    @Override
    public String process(Map<String, Object> elParams) {
        logger.info("Request to CreateController with params" + params);

        String result = operationHandler.handle(params);
        System.out.println(params);
        return result;
    }

}
