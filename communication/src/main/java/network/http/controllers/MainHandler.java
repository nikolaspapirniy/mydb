package network.http.controllers;

import com.sun.net.httpserver.HttpHandler;
import network.http.handlers.AbstractSimpleHandler;
import network.http.annotations.MyController;
import org.apache.log4j.Logger;

import java.util.Map;

@MyController(path = "/test")
public class MainHandler extends AbstractSimpleHandler {

    private static Logger logger = Logger.getLogger(MainHandler.class);

    @Override
    public String process(Map<String, Object> elParams) {
        elParams.put("", "");
        return "temp.html";
    }

}
