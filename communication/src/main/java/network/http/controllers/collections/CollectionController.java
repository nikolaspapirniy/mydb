package network.http.controllers.collections;


import network.http.annotations.MyController;
import network.http.handlers.AbstractSimpleHandler;
import org.apache.log4j.Logger;

import java.util.Map;

@MyController(path = "/collections")
public class CollectionController extends AbstractSimpleHandler {

    private static Logger logger = Logger.getLogger(CollectionController.class);

    @Override
    public String process(Map<String, Object> elParams) {
        logger.info("Request to CreateController with params" + params);
        return "list_of_collections.html";
    }
}
