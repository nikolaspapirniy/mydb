package network.http.content;

import org.apache.log4j.Logger;
import properties.AppProperties;
import properties.AppPropertyValues;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class StaticContentManager {
    private static Logger logger = Logger.getLogger(StaticContentManager.class);


    private List<String> foldersToScan = AppProperties.getPropertyAsList(AppPropertyValues.STATIC_CONTENT_FOLDERS_TO_SCAN);
    private static StaticContentManager instance;

    private Map<String, StaticTextEntity> textEntitiesMap = new ConcurrentHashMap<String, StaticTextEntity>();

    private StaticContentManager() {
    }

    public static StaticContentManager getInstance() {
        if (instance == null) {
            synchronized (StaticContentManager.class) {
                if (instance == null) {
                    instance = new StaticContentManager();
                }
            }
        }
        return instance;
    }

    public boolean addTextEntityToManager(String fileName, String filePath) {
        try {
            return tryToAddTextEntityToManager(fileName, filePath);
        } catch (IOException e) {
            logger.error("Couldn't add " + filePath + " to manager via IOException");
            return false;
        }
    }

    private boolean tryToAddTextEntityToManager(String fileName, String filePath) throws IOException {
        StaticTextEntity staticTextEntity = new StaticTextEntity(this, fileName, filePath);
        textEntitiesMap.put(fileName, staticTextEntity);
        logger.debug("Added file: " + fileName + " with path: " + filePath + " to manager");
        return true;
    }

    public String getTextEntityFromManager(String fileName) throws FileNotFoundException {
        synchronized (textEntitiesMap) {
            StaticTextEntity staticTextEntity = textEntitiesMap.get(fileName);
            if (isNotLoadedToManager(staticTextEntity))
                return tryToLoadToManager(fileName);
            else
                return tryToUpload(fileName, staticTextEntity);
        }
    }

    private String tryToUpload(String fileName, StaticTextEntity staticTextEntity) throws FileNotFoundException {
        try {
            staticTextEntity.checkAndUpload();
        } catch (FileNotFoundException e) {
            logger.error("FILE " + fileName + " HAS BEEN DELETED ERROR");
            throw new FileNotFoundException();
        }
        return staticTextEntity.getContent();
    }

    private String tryToLoadToManager(String fileName) {
        if (tryToLoadFile(fileName, null))
            return textEntitiesMap.get(fileName).getContent();
        else
            return null;
    }

    public void loadAllFiles(String fileEnding) {
        for (String filePath : foldersToScan)
            loadAllFilesInFolder(fileEnding, filePath);
    }

    private void loadAllFilesInFolder(String fileEnding, String filePath) {
        File file = new File(filePath);
        if (isValidExistingDirectory(file))
            loadAllFilesFromPath(fileEnding, file);

    }

    private void loadAllFilesFromPath(String fileEnding, File file) {
        for (File f : file.listFiles())
            loadFromPath(f.getName(), f.getAbsolutePath(), fileEnding);
    }

    private boolean isValidExistingDirectory(File file) {
        return file != null && file.isDirectory();
    }

    private boolean isNotLoadedToManager(StaticTextEntity staticTextEntity) {
        return staticTextEntity == null;
    }

    private boolean tryToLoadFile(String fileName, String fileEnd) {
        for (String filePath : foldersToScan) {
            if (loadFromPath(fileName, filePath, fileEnd))
                return true;
        }
        return false;
    }

    private boolean loadFromPath(String fileName, String filePath, String fileEnd) {
        if (isFileExistsInPath(fileName, filePath) && isFileTypeValid(fileName, fileEnd))
            return addTextEntityToManager(fileName, filePath);
        else
            return false;
    }

    private boolean isFileTypeValid(String fileName, String fileEndsWith) {
        if (fileEndsWith == null)
            return true;
        else
            return fileName.endsWith(fileEndsWith);

    }

    private boolean isFileExistsInPath(String fileName, String filePath) {
        File file = new File(filePath);
        return isValidExistingFile(file);
    }

    private boolean isValidExistingFile(File file) {
        return file.exists() && file.isFile();
    }

}
