package network.http.content;

import org.apache.log4j.Logger;
import repository.old.HeadRepository;
import repository.old.SystemConfig;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;

public class StaticTextEntity {
    private static Logger logger = Logger.getLogger(StaticTextEntity.class);

    private String fileName;
    private String filePath;
    private String content;
    private long lastUploadedDate;
    private long lastModifiedTimeStamp;
    private StaticContentManager manager;

    public StaticTextEntity(StaticContentManager manager, String fileName, String filePath) throws IOException {
        this.manager = manager;
        this.fileName = fileName;
        this.filePath = filePath;
        readFile(filePath);
    }

    private void readFile(String filePath) throws IOException {
        content = StaticContentUtils.readStaticFromFile(filePath);
        lastUploadedDate = Calendar.getInstance().getTimeInMillis();
        lastModifiedTimeStamp = defineLastModifiedTimeStamp();
    }

    public boolean checkAndUpload() throws FileNotFoundException {
        checkFileExist();

        if (isFileModified())
            return reuploadFile();
        else
            return false;
    }

    private boolean reuploadFile() throws FileNotFoundException {
        try {
            return tryToReuploadFile();
        } catch (IOException e) {
            logger.error("Error during update: " + filePath);
            throw new FileNotFoundException();
        }
    }

    private boolean tryToReuploadFile() throws IOException {
        readFile(filePath);
        logger.info("Reuploaded file: " + filePath);
        return true;
    }

    private boolean isFileModified() {
        return defineLastModifiedTimeStamp() != lastModifiedTimeStamp;
    }

    private void checkFileExist() throws FileNotFoundException {
        if (isFileExists() == false)
            throw new FileNotFoundException();
    }

    private boolean isFileExists() {
        File f = new File(filePath);
        return (f.exists() && f.isFile());
    }

    private long defineLastModifiedTimeStamp() {
        File f = new File(filePath);
        return f.lastModified();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public long getLastUploadedDate() {
        return lastUploadedDate;
    }

    public void setLastUploadedDate(long lastUploadedDate) {
        this.lastUploadedDate = lastUploadedDate;
    }

    public long getLastModifiedTimeStamp() {
        return lastModifiedTimeStamp;
    }

    public void setLastModifiedTimeStamp(long lastModifiedTimeStamp) {
        this.lastModifiedTimeStamp = lastModifiedTimeStamp;
    }

    public StaticContentManager getManager() {
        return manager;
    }

    public void setManager(StaticContentManager manager) {
        this.manager = manager;
    }

}
