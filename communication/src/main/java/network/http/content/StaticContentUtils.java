package network.http.content;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class StaticContentUtils {
    private static Logger logger = Logger.getLogger(StaticContentUtils.class);

    public static String readStaticFromFile(String filename) throws IOException {
        BufferedReader reader = null;
        try {
            File f = new File(filename);
            reader = new BufferedReader(new FileReader(f));
            return readFile(reader);
        } finally {
            closeReader(filename, reader);
        }
    }

    private static String readFile(BufferedReader reader) throws IOException {
        StringBuilder builder = new StringBuilder();
        String buff = null;
        while ((buff = reader.readLine()) != null)
            addLine(builder, buff);
        return builder.toString();
    }

    private static void addLine(StringBuilder builder, String buff) {
        builder.append(buff);
        builder.append(System.getProperty("line.separator"));
    }

    private static void closeReader(String filename, BufferedReader reader) {
        try {
            if (reader != null)
                reader.close();
        } catch (IOException e) {
            logger.error("Couldn't close file reader: " + filename);
        }
    }
}
