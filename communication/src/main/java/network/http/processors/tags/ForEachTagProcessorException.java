package network.http.processors.tags;

/**
 * Created by nikolay.papirniy on 6/12/14.
 */
public class ForEachTagProcessorException extends RuntimeException {
    public ForEachTagProcessorException() {
        super();
    }

    public ForEachTagProcessorException(String message) {
        super(message);
    }

    public ForEachTagProcessorException(String message, Throwable cause) {
        super(message, cause);
    }
}
