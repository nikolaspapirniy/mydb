package network.http.processors.tags;

import network.http.processors.el.ElProcessor;
import org.jdom.*;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Map;

public class ForEachTagProcessor {

    public static final String FOR_EACH_TAG = "forEach";
    public static final String ITEMS_ATTRIBUTE = "items";
    public static final String VAR_ATTRIBUTE = "var";

    public static String process(String text, Map<String, Object> params) {
        SAXBuilder builder = new SAXBuilder();
        try {
            Document document = builder.build(new StringReader(text));
            Element rootNode = document.getRootElement();

            processAllElements(rootNode.getChildren(), params);

            XMLOutputter xmlOutput = new XMLOutputter();

            String result = xmlOutput.outputString(document);
            return result.substring(40, result.length()).trim();
        } catch (IOException io) {
            System.out.println(io.getMessage());
        } catch (JDOMException jdomex) {
            System.out.println(jdomex.getMessage());
        }
        return null;
    }

    private static void processAllElements(List list, Map<String, Object> params) {
        for (int i = 0; i < list.size(); i++) {
            Element node = (Element) list.get(i);
            processXmlElement(node, params);
            processAllElements(node.getChildren(), params);
        }
    }

    private static Element processXmlElement(Element node, Map<String, Object> params) {
        if (FOR_EACH_TAG.equalsIgnoreCase(node.getName())) {
            processForEachTag(node, params);
        }
        return node;
    }

    private static void processForEachTag(Element node, Map<String, Object> params) {
        Element parent = (Element) node.getParent();
        parent.removeChild(FOR_EACH_TAG);

        List<Content> nodeContent = node.getContent();
        Attribute items = node.getAttribute(ITEMS_ATTRIBUTE);
        Attribute var = node.getAttribute(VAR_ATTRIBUTE);
        assertThatItemsAttrPresent(items);
        assertThatVarAttrPresent(var);

        String valueOfForEach = node.getValue();
        node.removeContent();

        List collection = (List) params.get(items.getValue());
        for (Object o : collection) {
            params.put(var.getValue(), o);
            String result = ElProcessor.replaceElToValues(valueOfForEach, params);
            nodeContent.add(new Text(result));
        }

        for (Content e : nodeContent) {
            parent.getContent().add(e.clone());
        }
    }

    private static void assertThatVarAttrPresent(Attribute var) {
        if(var == null)
            throw new ForEachTagProcessorException("Items attribute should be present");
    }

    private static void assertThatItemsAttrPresent(Attribute items) {
        if(items == null)
            throw new ForEachTagProcessorException("Items attribute should be present");
    }
}
