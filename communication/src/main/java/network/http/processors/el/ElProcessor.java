package network.http.processors.el;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ElProcessor {

    public static final String EL_PATTERN = "\\$\\{( )*\\S+( )*\\}";

    /**
     * Replaces expression languages forms to a simple text ${myparam}
     *
     * @param text          where to look for el constuctions
     * @param replaceParams map param values associated with param names
     * @return string with replaced el constructions
     */
    public static String replaceElToValues(String text, Map<String, Object> replaceParams) {
        return replaceElToValues(text, replaceParams, "null");
    }

    /**
     * Replaces expression languages forms to a simple text ${myparam}
     *
     * @param text           where to look for el constuctions
     * @param replaceParams  map param values associated with param names
     * @param noParamMessage string to be replace to if there is no param for el value
     * @return string with replaced el constructions
     */
    public static String replaceElToValues(String text, Map<String, Object> replaceParams, String noParamMessage) {
        StringBuilder builder = new StringBuilder(text);
        Pattern pattern = Pattern.compile(EL_PATTERN);
        Matcher matcher = pattern.matcher(text);

        // delta in string builder and param string (after replacing)
        int generalDelta = 0;
        // delta between found character indexces (${found_param})
        int foundDelta = 0;

        // start index of found word
        int start = 0;
        // end index of found word
        int end = 0;
        // SB for deleting pre and post characters like ${ and }
        StringBuilder foundStringBuilder = new StringBuilder();
        while (matcher.find()) {
            // crear sb
            foundStringBuilder.delete(0, foundStringBuilder.length());
            start = matcher.start();
            end = matcher.end();

            foundStringBuilder.append(matcher.group());
            deleteStartBraces(foundStringBuilder);
            deleteEndBraces(foundStringBuilder);

            trimSpacesOfElParameter(foundStringBuilder);

            String elParam = foundStringBuilder.toString();
            String paramStr;
            paramStr = getCompiledElParameter(replaceParams, noParamMessage, elParam);

            builder.replace(generalDelta + start, generalDelta + end, paramStr);

            foundDelta = paramStr.length() - (end - start);
            generalDelta += foundDelta;
        }
        return builder.toString();
    }

    private static String getCompiledElParameter(Map<String, Object> replaceParams, String noParamMessage, String elParam) {
        String[] splittedElParameter = elParam.split("\\.");
        String paramStr;
        switch (splittedElParameter.length) {
            case 1:
                paramStr = getCompiledElWithSingleParameter(replaceParams, noParamMessage, elParam);
                break;
            case 2:
                paramStr = getCompliedElWithProperty(replaceParams, noParamMessage, splittedElParameter);
                break;
            default:
                throw new ElException("Not correct expression");
        }
        return paramStr;
    }

    private static String getCompiledElWithSingleParameter(Map<String, Object> replaceParams, String noParamMessage, String elParam) {
        Object elParamValue = replaceParams.get(elParam);
        return (elParamValue == null) ? noParamMessage : elParamValue.toString();
    }

    private static String getCompliedElWithProperty(Map<String, Object> replaceParams, String noParamMessage, String[] split) {
        String paramStr;
        Object paramValue = replaceParams.get(getElParameterName(split));
        if (paramValue == null) {
            paramStr = noParamMessage;
        } else {
            paramStr = invokeGetterMethod(split, paramValue);
        }
        return paramStr;
    }

    private static String invokeGetterMethod(String[] split, Object paramValue)  {
        try {
            return (String) paramValue.getClass().getMethod(generateGetMethod(getPropertyName(split))).invoke(paramValue);
        } catch (NoSuchMethodException e) {
            throw new ElException("Not correct method call " + getElParameterName(split) + " " + getPropertyName(split));
        } catch (InvocationTargetException e) {
            throw new ElException("Not correct method call " + getElParameterName(split) + " " + getPropertyName(split));
        } catch (IllegalAccessException e) {
            throw new ElException("Not correct method call " + getElParameterName(split) + " " + getPropertyName(split));
        }
    }

    private static String getElParameterName(String[] split) {
        return split[0];
    }

    private static String getPropertyName(String[] split) {
        return split[1];
    }

    private static String generateGetMethod(String key) {
        StringBuilder sb = new StringBuilder();
        sb.append("get");
        sb.append(key.substring(0, 1).toUpperCase());
        sb.append(key.substring(1, key.length()));
        return sb.toString();
    }

    private static void deleteEndBraces(StringBuilder foundStringBuilder) {
        foundStringBuilder.delete(foundStringBuilder.length() - 1, foundStringBuilder.length());
    }

    private static void deleteStartBraces(StringBuilder foundStringBuilder) {
        foundStringBuilder.delete(0, 2);
    }

    private static void trimSpacesOfElParameter(StringBuilder foundStringBuilder) {
        String trimmedParameter = foundStringBuilder.toString().trim();
        foundStringBuilder.replace(0, foundStringBuilder.length(), trimmedParameter);
    }

    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("\\$\\{( )*\\S+( )*\\}");
        Matcher matcher = pattern.matcher("${asda.name}");

        while (matcher.find()) {
            System.out.println(matcher.group());
        }
    }
}
