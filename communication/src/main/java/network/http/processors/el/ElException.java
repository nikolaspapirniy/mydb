package network.http.processors.el;

/**
 * Created by nikolay.papirniy on 6/11/14.
 */
public class ElException extends RuntimeException {

    public ElException() {
        super();
    }

    public ElException(String message) {
        super(message);
    }

    public ElException(String message, Throwable cause) {
        super(message, cause);
    }
}
