package network.http.handlers;


import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import properties.AppProperties;
import properties.AppPropertyValues;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractHttpHandler implements HttpHandler {
    public static final String PARAMETERS_ATTRIBUTE = AppProperties.getPropertyAsString(AppPropertyValues.HTTP_PARAMETERS_ATTRIBUTE);
    protected Map<String, Object> params = new HashMap<String, Object>();

    protected void sendResponse(HttpExchange exchange, String responseString) throws IOException {
        exchange.sendResponseHeaders(200, responseString.length());
        OutputStream os = exchange.getResponseBody();
        os.write(responseString.getBytes());
        os.close();
    }

    public abstract String process(Map<String, Object> elParams);
}
