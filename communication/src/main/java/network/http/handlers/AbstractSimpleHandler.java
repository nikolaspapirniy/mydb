package network.http.handlers;

import com.sun.net.httpserver.HttpExchange;
import network.http.processors.el.ElProcessor;
import org.apache.log4j.Logger;
import network.http.content.StaticContentManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractSimpleHandler extends AbstractHttpHandler {
    private static Logger logger = Logger.getLogger(AbstractSimpleHandler.class);
    protected static StaticContentManager staticContentManager = StaticContentManager.getInstance();

    static {
        initStaticContent();
    }

    public static void initStaticContent() {
        staticContentManager.loadAllFiles(".html");
        //staticContentManager.addTextEntityToManager("temp.html", HeadRepository.STATIC_FILES_FOLDER_NAME + File.separator + "temp.html");
        //staticContentManager.addTextEntityToManager("test_page.html", HeadRepository.STATIC_FILES_FOLDER_NAME + File.separator + "test_page.html");
    }

    public void handle(HttpExchange exchange) throws IOException {
        params = (Map<String, Object>) exchange.getAttribute(PARAMETERS_ATTRIBUTE);

        Map<String, Object> elParams = new HashMap<>();
        String pageName = process(elParams);
        String responseString = renderPage(pageName, elParams);

        sendResponse(exchange, responseString);
    }
    private String renderPage(String pageName, Map<String, Object> elParams) throws FileNotFoundException {
        String resultString = staticContentManager.getTextEntityFromManager(pageName);
        if (resultString == null)
            return getFileNotFoundPage(pageName);
        else
            return ElProcessor.replaceElToValues(resultString, elParams);
    }

    private String getFileNotFoundPage(String pageName) {
        String errorMessage = "File " + pageName + " not found!";
        logger.error(errorMessage);
        return errorMessage;
    }
}
