package network.http.handlers;

import com.sun.net.httpserver.HttpExchange;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractAjaxHandler extends AbstractHttpHandler {
    private static Logger logger = Logger.getLogger(AbstractAjaxHandler.class);

    public void handle(HttpExchange exchange) throws IOException {
        params = (Map<String, Object>) exchange.getAttribute(PARAMETERS_ATTRIBUTE);

        Map<String, Object> elParams = new HashMap<>();
        String responseString = process(elParams);

        sendResponse(exchange, responseString);
    }
}
