package network.http.filters;

import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpExchange;
import network.http.annotations.MyFilter;
import org.apache.log4j.Logger;
import properties.AppProperties;
import properties.AppPropertyValues;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@MyFilter
public class ParametersFilter extends Filter {
    public static final String PARAMETERS_ATTRIBUTE = AppProperties.getPropertyAsString(AppPropertyValues.HTTP_PARAMETERS_ATTRIBUTE);
    private static Logger logger = Logger.getLogger(ParametersFilter.class);
    public static final String UTF_8 = "utf-8";

    @Override
    public String description() {
        return "ParametersFilter";
    }

    @Override
    public void doFilter(HttpExchange exchange, Chain chain) throws IOException {
        parseGetParameters(exchange);
        if (isPost(exchange))
            parsePostParameters(exchange);
        chain.doFilter(exchange);
    }

    private boolean isPost(HttpExchange exchange) {
        return "post".equalsIgnoreCase(exchange.getRequestMethod());
    }

    private void parseGetParameters(HttpExchange exchange) throws UnsupportedEncodingException {
        exchange.setAttribute(PARAMETERS_ATTRIBUTE, retrieveGetHttpParameters(exchange));
    }

    private Map<String, Object> retrieveGetHttpParameters(HttpExchange exchange) throws UnsupportedEncodingException {
        Map<String, Object> parameters = new HashMap<String, Object>();
        URI requestedUri = exchange.getRequestURI();
        String query = requestedUri.getRawQuery();
        QueryParser.parseQuery(query, parameters);
        return parameters;
    }

    private void parsePostParameters(HttpExchange exchange) throws IOException {
        @SuppressWarnings("unchecked")
        Map<String, Object> parameters = (Map<String, Object>) exchange.getAttribute(PARAMETERS_ATTRIBUTE);
        InputStreamReader isr = new InputStreamReader(exchange.getRequestBody(), UTF_8);
        BufferedReader br = new BufferedReader(isr);
        String query = br.readLine();
        QueryParser.parseQuery(query, parameters);
        closeStream(isr);
    }

    private void closeStream(InputStreamReader isr) {
        try {
            if (isr != null)
                isr.close();
        } catch (IOException e) {
            logger.error("Can't close input stream in parameterFilter");
        }
    }
}
