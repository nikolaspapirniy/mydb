package network.http.filters;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class QueryParser {
    public static final String HTTP_PARAM_SEPARATOR = "=";
    public static final String HTTP_PARAMS_DELIMITER = "&";

    @SuppressWarnings("unchecked")
    public static void parseQuery(String query, Map<String, Object> parameters) throws UnsupportedEncodingException {
        if (query == null)
            return;
        else
            parseNotNullQuery(query, parameters);

    }

    private static void parseNotNullQuery(String query, Map<String, Object> parameters) throws UnsupportedEncodingException {
        parseAllParams(parameters, query.split(HTTP_PARAMS_DELIMITER));
    }

    private static void parseAllParams(Map<String, Object> parameters, String[] pairs) throws UnsupportedEncodingException {
        for (String pair : pairs)
            parseParameter(parameters, pair);
    }

    private static void parseParameter(Map<String, Object> parameters, String pair) throws UnsupportedEncodingException {
        String param[] = pair.split(HTTP_PARAM_SEPARATOR);

        String key = getKeyFromParam(param);
        String value = getValueFromParam(param);

        if (parameters.containsKey(key))
            addMultipleParamsWithOneKey(parameters, key, value);
        else
            parameters.put(key, value);
    }

    private static void addMultipleParamsWithOneKey(Map<String, Object> parameters, String key, String value) {
        Object obj = parameters.get(key);
        if (isListAlreadyExists(obj))
            ((List) obj).add(value);
        else if (isSingleValue(obj))
            parameters.put(key, createListOfValues(value, obj));
    }

    private static List createListOfValues(String value, Object previousObject) {
        List values = new ArrayList();
        values.add(previousObject);
        values.add(value);
        return values;
    }

    private static boolean isSingleValue(Object obj) {
        return obj instanceof String;
    }

    private static boolean isListAlreadyExists(Object obj) {
        return obj instanceof List;
    }

    private static String getValueFromParam(String[] param) throws UnsupportedEncodingException {
        if (containsValue(param))
            return decodeParam(getValueFromPair(param));
        else
            return null;
    }

    private static String getValueFromPair(String[] param) {
        return param[1];
    }

    private static boolean containsValue(String[] param) {
        return param.length > 1;
    }

    private static String getKeyFromParam(String[] param) throws UnsupportedEncodingException {
        if (containsKey(param))
            return decodeParam(getKeyFromPair(param));
        else
            return null;
    }

    private static String getKeyFromPair(String[] param) {
        return param[0];
    }

    private static String decodeParam(String s) throws UnsupportedEncodingException {
        return URLDecoder.decode(s, System.getProperty("file.encoding"));
    }

    private static boolean containsKey(String[] param) {
        return param.length > 0;
    }
}
