package network.http.loaders;


import com.sun.net.httpserver.Filter;
import network.http.annotations.MyFilter;
import org.apache.log4j.Logger;
import org.reflections.Reflections;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FilterLoader {
    private static Logger logger = Logger.getLogger(FilterLoader.class);

    public static List<Filter> loadAnnotatedFilters(List<String> filtersPackages) {
        List<Filter> filters = new ArrayList<Filter>();
        for (String filtersPackage : filtersPackages)
            initAnnotationFiltersInPackage(filtersPackage, filters);
        return filters;
    }

    private static List<Filter> initAnnotationFiltersInPackage(String filtersPackage, List<Filter> filters) {
        Reflections reflections = new Reflections(filtersPackage);
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(MyFilter.class);

        for (Class filter : annotated)
            safelyAddToList(filters, filter);

        return filters;
    }

    private static void safelyAddToList(List<Filter> filters, Class filter) {
        try {
            filters.add((Filter) filter.newInstance());
            logger.info("Added filter " + filter);
        } catch (InstantiationException e) {
            throw new RuntimeException("Can't instantiate filter: " + filter.toString(), e);
        } catch (ClassCastException e) {
            throw new RuntimeException("filter: " + filter.toString() + " is not Filter", e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("filter: " + filter.toString() + " is not accessible", e);
        }
    }
}
