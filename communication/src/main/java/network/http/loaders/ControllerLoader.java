package network.http.loaders;

import com.sun.net.httpserver.HttpHandler;
import network.http.annotations.MyController;
import org.apache.log4j.Logger;
import org.reflections.Reflections;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ControllerLoader {
    private static Logger logger = Logger.getLogger(ControllerLoader.class);

    public static Map<String, HttpHandler> loadAnnotatedControllers(List<String> controllerPackages) {
        Map<String, HttpHandler> bindings = new HashMap<String, HttpHandler>();
        for (String controllerPackageName : controllerPackages)
            initAnnotationControllerInPackage(controllerPackageName, bindings);
        return bindings;
    }

    private static void initAnnotationControllerInPackage(String controllersPackage, Map<String, HttpHandler> bindingsToAddress) {
        Reflections reflections = new Reflections(controllersPackage);
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(MyController.class);

        for (Class<?> controller : annotated)
            addBinding(bindingsToAddress, controller);
    }

    private static void addBinding(Map<String, HttpHandler> bindingsToAddress, Class<?> controller) {
        MyController myController = controller.getAnnotation(MyController.class);
        String mapping = myController.path();
        safelyAddToBinding(bindingsToAddress, controller, mapping);
    }

    private static void safelyAddToBinding(Map<String, HttpHandler> bindingsToAddress, Class<?> controller, String mapping) {
        try {
            bindingsToAddress.put(mapping, (HttpHandler) controller.newInstance());
            logger.info("Added mapping for " + mapping);
        } catch (InstantiationException e) {
            throw new RuntimeException("Can't instantiate controller: " + controller.toString(), e);
        } catch (ClassCastException e) {
            throw new RuntimeException("Controller: " + controller.toString() + " is not HttpHandler", e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Controller: " + controller.toString() + " is not accessible", e);
        }
    }
}
