package network.http;

import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import network.http.loaders.ControllerLoader;
import network.http.loaders.FilterLoader;
import org.apache.log4j.Logger;
import properties.AppProperties;
import properties.AppPropertyValues;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

public class HttpServerCore implements Runnable {
    private static Logger logger = Logger.getLogger(HttpServerCore.class);

    private HttpServer serverInstance;
    private long port = AppProperties.getPropertyAsNumber(AppPropertyValues.HTTP_PORT);
    private List<String> controllerPackages = AppProperties.getPropertyAsList(AppPropertyValues.PACKAGES_WITH_CONTROLLERS);
    private List<String> filterPackages = AppProperties.getPropertyAsList(AppPropertyValues.PACKAGES_WITH_FILTERS);

    public HttpServerCore() {
        startMessage();
    }

    public HttpServerCore(int port) {
        super();
        this.port = port;
        startMessage();
    }

    private void startMessage() {
        try {
            logger.info("MyDB started on host: " + InetAddress.getLocalHost().getCanonicalHostName() + ":" + port);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("restriction")
    public void run() {
        try {
            if (port > 65500)
                throw new RuntimeException("Port value is illegal");
            serverInstance = HttpServer.create(new InetSocketAddress((int) port), 0);
            serverInit();
            serverInstance.start();
            logger.info("Server started!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("restriction")
    private void serverInit() {
        serverInstance.setExecutor(null); // creates a default executor
        initMappingHandlers();
    }

    @SuppressWarnings("restriction")
    private void initMappingHandlers() {
        Map<String, HttpHandler> bindings = ControllerLoader.loadAnnotatedControllers(controllerPackages);
        List<Filter> filters = FilterLoader.loadAnnotatedFilters(filterPackages);
        createContextForMapping(bindings, filters);
    }

    private void createContextForMapping(Map<String, HttpHandler> bindings, List<Filter> filters) {
        for (Map.Entry<String, HttpHandler> entry : bindings.entrySet())
            createHttpContextForBinding(filters, entry);
    }

    private void createHttpContextForBinding(List<Filter> filters, Map.Entry<String, HttpHandler> entry) {
        HttpContext binding = serverInstance.createContext(entry.getKey(), entry.getValue());
        addFilters(binding, filters);
    }

    private void addFilters(HttpContext createContext, List<Filter> filters) {
        for (Filter f : filters)
            createContext.getFilters().add(f);
    }
}
